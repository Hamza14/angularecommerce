import { BrowserModule } from '@angular/platform-browser';
import {NgModule, Renderer2} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { FooterComponent } from './footer/footer.component';

import {HttpClientModule} from '@angular/common/http';
import {CategorieComponent} from './categorie/categorie.component';
import { AppRoutingModule } from './app-routing.module';
import { UniquePipe } from './unique.pipe';
import { ProductComponent } from './product/product.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { CustomerComponent } from './customer/customer.component';
import { CartComponent } from './cart/cart.component';
import {FormsModule} from '@angular/forms';
import { CheckoutComponent } from './checkout/checkout.component';
import {SWIPER_CONFIG, SwiperConfigInterface, SwiperModule} from 'ngx-swiper-wrapper';
import {ReactiveFormsModule} from '@angular/forms';
import {CatalogEndpointService} from './api/services/catalog-endpoint.service';
import { AccountComponent } from './account/account.component';
import { AddressComponent } from './address/address.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { MyPaymentsComponent } from './my-payments/my-payments.component';
import {AuthGuard} from './auth.guard';
import { ThanksComponent } from './thanks/thanks.component';



const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CategorieComponent,
    UniquePipe,
    ProductComponent,
    CustomerComponent,
    CartComponent,
    CheckoutComponent,
    AccountComponent,
    AddressComponent,
    MyOrdersComponent,
    MyPaymentsComponent,
    ThanksComponent,


  ],
  imports: [
    BrowserModule,
    NgbModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    AppRoutingModule,
    NgxPaginationModule,
    FormsModule,
    SwiperModule,
    ReactiveFormsModule

  ],
  providers: [ {
    provide: SWIPER_CONFIG,
    useValue: DEFAULT_SWIPER_CONFIG
  } ,
  CategorieComponent,
    HeaderComponent,
    AuthGuard,
  CatalogEndpointService],

  bootstrap: [AppComponent]
})
export class AppModule { }
