import {Component, Input, OnInit, Renderer2} from '@angular/core';


import {CatalogEndpointService} from '../api/services/catalog-endpoint.service';
import * as $ from 'node_modules/jquery';


import {CategoryWrapper} from '../api/models/category-wrapper';
import FindAllCategoriesUsingGET1Params = CatalogEndpointService.FindAllCategoriesUsingGET1Params;
import {CategoriesWrapper} from '../api/models/categories-wrapper';
import {Router} from '@angular/router';
import FindProductByIdUsingGET1Params = CatalogEndpointService.FindProductByIdUsingGET1Params;
import {ProductWrapper} from '../api/models/product-wrapper';
import {CartService} from '../cart.service';
import {CartEndpointService} from '../api/services/cart-endpoint.service';
import {CustomCartEndpointService} from '../api/services/custom-cart-endpoint.service';

import {OrderItemWrapper} from '../api/models/order-item-wrapper';
import {OrderWrapper} from '../api/models/order-wrapper';
import RemoveItemFromOrderUsingDELETE1Params = CartEndpointService.RemoveItemFromOrderUsingDELETE1Params;
import {Money} from '../api/models/money';
import {CategorieComponent} from '../categorie/categorie.component';

import {FormControl} from '@angular/forms';
import FindSearchResultsByQueryUsingGET1Params = CatalogEndpointService.FindSearchResultsByQueryUsingGET1Params;
import {SearchResultsWrapper} from '../api/models/search-results-wrapper';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  categories: CategoriesWrapper;
  products: ProductWrapper[];
  product: ProductWrapper;
  cartProducts: OrderItemWrapper[];
  cartProduct: OrderItemWrapper;
  order: OrderWrapper;
  cart : OrderWrapper;
  total: Money;
  username:string ;
  results: ProductWrapper[];
  result: ProductWrapper;
  queryField: FormControl = new FormControl();
  private imgUrl = 'http://localhost:8081/admin';
  private categorie: CategoryWrapper;


  constructor(private catalogEndpointService: CatalogEndpointService,
              private router: Router,
              private renderer: Renderer2,
              private cartService: CartService,
              private cartEndpointService: CartEndpointService,
              private customCartEndpointService: CustomCartEndpointService) {
  }

  params(params: FindAllCategoriesUsingGET1Params) {
    return params;
  }

  params1(params: FindSearchResultsByQueryUsingGET1Params) {
    return params;
  }

  params2(params: RemoveItemFromOrderUsingDELETE1Params) {
    return params;
  }

  orderItem(orderItems: OrderItemWrapper) {
    return orderItems;
  }

  /*
   order(order: OrderWrapper){
     return order;
   }
  */
  ngOnInit() {
this.username = localStorage.getItem("name");
    let customerId = JSON.parse(localStorage.getItem('customerId'));
    this.customCartEndpointService.findCartForCustomerUsingGET1(customerId)
      .subscribe(cart=>{
        this.cart=cart;
      });
    this.catalogEndpointService.findAllCategoriesUsingGET1(this.params({}))
      .subscribe(data => {
        this.categories = JSON.parse(JSON.stringify(data)).categories;
      });
    this.cartInit();
    this.customCartEndpointService.watchCart()
      .subscribe(() => {
        this.cartInit();
      });


    this.queryField.valueChanges
      .subscribe(queryField => {
        this.catalogEndpointService.findSearchResultsByQueryUsingGET1(this.params1({q: queryField}))
          .subscribe(response => {
            this.results = JSON.parse(JSON.stringify(response)).products;
          });
      });


  }

  selectedCategorie(id: number) {

    this.router.navigate(['/categorie', id]);

  }

  //remove item from localStorage cart
  removeItem(id: number) {
    let customerId = JSON.parse(localStorage.getItem('customerId'));
    this.cartEndpointService.removeItemFromOrderUsingDELETE1(this.params2({itemId: id, cartId: this.cart.id, customerId: customerId}))
      .subscribe(data => {
        this.ngOnInit();
      });

  }

  selectedProduct(id: number) {

    this.router.navigate(['product', id]);
  }


  cartInit() {
    /* let cart = JSON.parse(localStorage.getItem('cart'));
     let products: any = [];
     for (let i = 0; i < cart.length; i++) {
       this.catalogEndpointService.findProductByIdUsingGET1(this.params1({'id': cart[i]}))
         .subscribe(data => {
           products.push(JSON.parse(JSON.stringify(data)));
           let total: number = 0;
           for (let i = 0; i < products.length; i++) {

             total = total + products[i].retailPrice.amount;
             this.total = total;
           }
         });

     }
     this.products = products;*/
    let customerId = JSON.parse(localStorage.getItem('customerId'));
    this.customCartEndpointService.findCartForCustomerUsingGET1(customerId)
      .subscribe(data => {

        this.cartProducts = JSON.parse(JSON.stringify(data.orderItems));
        this.total = data.total;


      });
  }

  checkOut() {

    /*this.customCartEndpointService.createNewCartForCustomerUsingPOST1(4641)
      .subscribe(data => {
        console.log(data);
        this.cartEndpointService.addItemToOrderUsingPOST1(
          this.params2({ 'orderItemWrapper': this.orderItem({'orderId':data.id}),'cartId': data.id, 'customerId': data.customer.id}))
          .subscribe(data2 => {
            console.log(data2);
          });
      });*/
  }

  async delay(ms: number) {
    await new Promise(resolve => setTimeout(()=>resolve(), ms)).then();
  }
  logout() {
    localStorage.removeItem("customerId");
    localStorage.removeItem("name");
    localStorage.removeItem("email");
    window.location.reload();


  }
}
