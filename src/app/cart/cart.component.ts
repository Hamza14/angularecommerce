import {Component, OnInit, Renderer2} from '@angular/core';
import {CategoriesWrapper} from '../api/models/categories-wrapper';
import {ProductWrapper} from '../api/models/product-wrapper';
import {CategoryWrapper} from '../api/models/category-wrapper';
import {CatalogEndpointService} from '../api/services/catalog-endpoint.service';
import {Router} from '@angular/router';
import {CartService} from '../cart.service';
import FindAllCategoriesUsingGET1Params = CatalogEndpointService.FindAllCategoriesUsingGET1Params;
import FindProductByIdUsingGET1Params = CatalogEndpointService.FindProductByIdUsingGET1Params;
import {CustomCartEndpointService} from '../api/services/custom-cart-endpoint.service';
import {OrderItemWrapper} from '../api/models/order-item-wrapper';
import {OrderWrapper} from '../api/models/order-wrapper';
import {Money} from '../api/models/money';
import {FormControl} from '@angular/forms';
import {CartEndpointService} from '../api/services/cart-endpoint.service';
import RemoveItemFromOrderUsingDELETE1Params = CartEndpointService.RemoveItemFromOrderUsingDELETE1Params;

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  categories: CategoriesWrapper;
  products : ProductWrapper[];
  product : ProductWrapper;
  cartProducts:OrderItemWrapper[];
  cartProduct:OrderItemWrapper;
  order: OrderWrapper;
  total: Money;
  results: ProductWrapper[];
  result:ProductWrapper;
  queryField: FormControl = new FormControl();

  private  imgUrl = 'http://localhost:8081/admin';
  private categorie: CategoryWrapper;


  constructor(private catalogEndpointService: CatalogEndpointService,
              private router: Router,
              private cartEndpointService:CartEndpointService,
              private customCartEndpointService:CustomCartEndpointService,
              private renderer: Renderer2,
              private cartService:CartService) {
  }

  params(params: FindAllCategoriesUsingGET1Params) {
    return params;
  }
  params2(params: RemoveItemFromOrderUsingDELETE1Params) {
    return params;
  }

  params1(params: FindProductByIdUsingGET1Params) {
    return params;
  }

  ngOnInit() {


    this.cartInit();



  /*  this.cartService.watchStorage().subscribe(()=>{
      this.cartInit();
    });*/

  }
  //remove item from localStorage cart
  removeItem(id: number) {
    this.cartEndpointService.removeItemFromOrderUsingDELETE1(this.params2({itemId:id,cartId:1,customerId:1}))
      .subscribe(data=>{
        this.ngOnInit();
      });

  }
  cartInit() {
    let customerId = JSON.parse(localStorage.getItem('customerId'));
    this.customCartEndpointService.findCartForCustomerUsingGET1(customerId)
      .subscribe(data => {

        this.cartProducts = JSON.parse(JSON.stringify(data.orderItems));
        this.total = data.total;


      });
  }
}
