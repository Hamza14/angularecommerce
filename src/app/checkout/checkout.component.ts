import { Component, OnInit } from '@angular/core';
import {CustomCartEndpointService} from '../api/services/custom-cart-endpoint.service';
import {CategoriesWrapper} from '../api/models/categories-wrapper';
import {ProductWrapper} from '../api/models/product-wrapper';
import {OrderItemWrapper} from '../api/models/order-item-wrapper';
import {OrderWrapper} from '../api/models/order-wrapper';
import {Money} from '../api/models/money';
import {FulfillmentEndpointService} from '../api/services/fulfillment-endpoint.service';
import {FulfillmentOptionWrapper} from '../api/models/fulfillment-option-wrapper';
import {FulfillmentEstimationWrapper} from '../api/models/fulfillment-estimation-wrapper';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  cartProducts:OrderItemWrapper[];
  cartProduct:OrderItemWrapper;
  order: OrderWrapper;
  total: Money;
  cartId:number;
  shippingPrice:Money;
  fulfillementOptions:FulfillmentEstimationWrapper[];
  option : FulfillmentEstimationWrapper;
  constructor(private customCartEndpointService: CustomCartEndpointService,
              private fulfillmentEndpointService:FulfillmentEndpointService) { }

  ngOnInit() {
    let customerId = JSON.parse(localStorage.getItem('customerId'));
    this.customCartEndpointService.findCartForCustomerUsingGET1(customerId)
      .subscribe(data=>{
        this.cartId=data.id;
        this.cartProducts=JSON.parse(JSON.stringify(data.orderItems));
        this.total=data.total;
      });
    this.fulfillmentEndpointService.getFulfillmentEstimationsUsingGET1(this.cartId)
      .subscribe(data=>{
        this.fulfillementOptions=JSON.parse(JSON.stringify(data));

      });

  }

}
