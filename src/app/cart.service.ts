import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {runModuleAsObservableFork} from '@angular-devkit/build-angular/src/utils';


@Injectable({
  providedIn: 'root'
})
export class CartService {
  private storageSub= new Subject<boolean>();

  constructor() {
  }
  watchCart(): Observable<any> {
    return this.storageSub.asObservable();
  }
  addToCart(id :number) {
    let cart: any = [];
    if (localStorage.getItem('cart') == null) {

      cart.push(JSON.stringify(id));
      localStorage.setItem('cart', JSON.stringify(cart));
    } else {
      let cart: any = JSON.parse(localStorage.getItem('cart'));
      let index: number = -1;
      for (let i = 0; i < cart.length; i++) {
        let item: number = JSON.parse(cart[i]);
        if (item == id) {
          index = i;
          break;
        }
      }
      if (index == -1) {
        cart.push(JSON.stringify(id));
        localStorage.setItem('cart', JSON.stringify(cart));
      } else {
        alert('item already in the cart');
      }
    }

    this.storageSub.next();
  }
  removeItem(id: number){
    let cart: any = JSON.parse(localStorage.getItem('cart'));

    for (let i = 0; i < cart.length; i++) {
      let item: number = JSON.parse(cart[i]);
      if (item == id) {
        cart.splice(i, 1);
        break;
      }
    }
    localStorage.setItem("cart", JSON.stringify(cart));
    this.storageSub.next();
  }
}
