import {Component, Input, OnInit, Output, Renderer2} from '@angular/core';
import {CatalogEndpointService} from '../api/services/catalog-endpoint.service';
import {ProductWrapper} from '../api/models/product-wrapper';
import FindSearchResultsByCategoryAndQueryUsingGET1Params = CatalogEndpointService.FindSearchResultsByCategoryAndQueryUsingGET1Params;
import {ActivatedRoute, Router} from '@angular/router';
import FindUpSaleProductsByProductUsingGET1Params = CatalogEndpointService.FindUpSaleProductsByProductUsingGET1Params;
import {CustomerEndpointService} from '../api/services/customer-endpoint.service';
import {CustomerWrapper} from '../api/models/customer-wrapper';
import {CartEndpointService} from '../api/services/cart-endpoint.service';
import {CartService} from '../cart.service';
import {HeaderComponent} from '../header/header.component';
import {CategoriesWrapper} from '../api/models/categories-wrapper';
import {CategoryWrapper} from '../api/models/category-wrapper';
import {OrderItemWrapper} from '../api/models/order-item-wrapper';
import {CustomCartEndpointService} from '../api/services/custom-cart-endpoint.service';

import AddItemToOrderUsingPOST3Params = CustomCartEndpointService.AddItemToOrderUsingPOST3Params;
import {observable, Subject} from 'rxjs';




@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.css']
})
export class CategorieComponent implements OnInit {
  @Input() products: ProductWrapper[];
  cartProducts: ProductWrapper[];
  product: ProductWrapper;
  categorie: CategoryWrapper;
  private clickStream = new Subject<Event>();

  @Output() observ = this.clickStream.asObservable();
  collection = [];
  imgUrl = 'http://localhost:8081/admin';

  params(params: FindSearchResultsByCategoryAndQueryUsingGET1Params) {
    return params;
  }

  params1(params: CustomerWrapper) {
    return params;
  }

  orderItem(orderItems: OrderItemWrapper) {
    return orderItems;
  }

  params2(params: AddItemToOrderUsingPOST3Params) {
    return params;
  }

  constructor(private catalogEndpointService: CatalogEndpointService,
              private customerEndpointService: CustomerEndpointService,
              private cartEndpointService: CartEndpointService,
              private customCartEndpointService: CustomCartEndpointService,
              private cartService: CartService,
              private route: ActivatedRoute,
              private router: Router,
              private renderer:Renderer2) {
    for (let i = 1; i <= 100; i++) {
      this.collection.push(`item ${i}`);
    }
  }




  ngOnInit() {

    this.route.params.subscribe(params => {
      this.catalogEndpointService.findCategoryByIdUsingGET1(
        this.params({'categoryId': params.id}))
        .subscribe(data => {

          this.products = JSON.parse(JSON.stringify(data)).products;

        });
      this.catalogEndpointService.findCategoryByIdUsingGET1({'categoryId': params.id})
        .subscribe(categorie => {
          this.categorie = JSON.parse(JSON.stringify(categorie));
        });
    });


  }

  selectedProduct(id: number) {
    this.router.navigate(['/product', id]);
  }

//adding item to cart
  addToCart(product : ProductWrapper,event:Event) {
    if(localStorage.getItem('customerId')== null){
      alert('please log in or create an account in order to add the item to the cart !');
      this.router.navigate(['/account']);
    }
    let customerId = JSON.parse(localStorage.getItem('customerId'));
    this.customCartEndpointService.findCartForCustomerUsingGET1(customerId).subscribe(cart=>{ this.customCartEndpointService.addItemToOrderUsingPOST3(
      this.params2({
        'customerId': customerId,
        'cartId': cart.id,
        'orderItemWrapper': this.orderItem({productId: product.id,
          orderId:cart.id,
          categoryId:product.defaultCategoryId,
          orderItemAttributes:product.productAttributes,
          retailPrice:product.retailPrice,
          productUrl:product.url})
      }))
      .subscribe(data2 => {

      });
    });

    this.clickStream.next(event);

  }

}
