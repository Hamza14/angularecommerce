import { NgModule } from '@angular/core';

import {RouterModule, Routes} from '@angular/router';
import {CategorieComponent} from './categorie/categorie.component';
import {ProductComponent} from './product/product.component';
import {CustomerComponent} from './customer/customer.component';
import {CartComponent} from './cart/cart.component';
import {CheckoutComponent} from './checkout/checkout.component';
import {AccountComponent} from './account/account.component';
import {AddressComponent} from './address/address.component';
import {MyOrdersComponent} from './my-orders/my-orders.component';
import {MyPaymentsComponent} from './my-payments/my-payments.component';
import {AuthGuard} from './auth.guard';
import {ThanksComponent} from './thanks/thanks.component';
const routes: Routes = [
  { path: 'categorie/:id', component: CategorieComponent },
  { path: 'product/:id', component: ProductComponent },
  { path: 'account', component: CustomerComponent },
  { path: 'cart', component: CartComponent },
  { path: 'thanks', component: ThanksComponent },
  { path: 'accountInfo', component: AccountComponent ,canActivate:[AuthGuard]},
  { path: 'address', component: AddressComponent,canActivate:[AuthGuard] },
  { path: 'myOrders', component: MyOrdersComponent ,canActivate:[AuthGuard]},
  { path: 'myPayments', component: MyPaymentsComponent,canActivate:[AuthGuard] },
  { path: 'checkout', component: CheckoutComponent }

];
@NgModule({

  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
