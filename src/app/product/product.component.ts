import {Component, Input, OnInit} from '@angular/core';
import {CatalogEndpointService} from '../api/services/catalog-endpoint.service';
import FindProductByIdUsingGET1Params = CatalogEndpointService.FindProductByIdUsingGET1Params;
import {ActivatedRoute} from '@angular/router';
import {ProductWrapper} from '../api/models/product-wrapper';
import FindUpSaleProductsByProductUsingGET1Params = CatalogEndpointService.FindUpSaleProductsByProductUsingGET1Params;
import {RelatedProductsEndpointService} from '../api/services/related-products-endpoint.service';
import GetRelatedProductsUsingGET1Params = RelatedProductsEndpointService.GetRelatedProductsUsingGET1Params;
import {SwiperConfigInterface, SwiperPaginationInterface, SwiperScrollbarInterface} from 'ngx-swiper-wrapper';



@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  @Input() product: ProductWrapper;
  @Input() upSaleProducts: ProductWrapper[];
  upSaleProduct: ProductWrapper;

  private  imgUrl = 'http://localhost:8081/admin';

  params(params: FindProductByIdUsingGET1Params) {
    return params;
  }

  params1(params: GetRelatedProductsUsingGET1Params) {
    return params;
  }

  constructor(private catalogEndpointService: CatalogEndpointService,
              private relatedProductsEndpointService:RelatedProductsEndpointService,
              private route: ActivatedRoute) {

  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.catalogEndpointService.findProductByIdUsingGET1(this.params({'id': params.id}))
        .subscribe(data => {
          this.product = JSON.parse(JSON.stringify(data));
        });
      this.relatedProductsEndpointService.getRelatedProductsUsingGET1(this.params1({'productId': params.id}))
        .subscribe(data => {
          this.upSaleProducts = JSON.parse(JSON.stringify(data)).upsaleProducts;
        });
    });

  }
  public show: boolean = true;

  public slides = [
    'imgUrl+product.primaryMedia.url',
    'imgUrl+product.primaryMedia.url',
    'imgUrl+product.primaryMedia.url',
    'imgUrl+product.primaryMedia.url'
  ];

  public type: string = 'component';

  public disabled: boolean = false;

  public config: SwiperConfigInterface = {
    direction: 'vertical',
    slidesPerView: 3,
    keyboard: true,
    mousewheel: true,
    scrollbar: false,
    navigation: true,
    pagination: false
  };

  private scrollbar: SwiperScrollbarInterface = {
    el: '.swiper-scrollbar',
    hide: false,
    draggable: true
  };

  private pagination: SwiperPaginationInterface = {
    el: '.swiper-pagination',
    clickable: true,
    hideOnClick: false
  };
}
