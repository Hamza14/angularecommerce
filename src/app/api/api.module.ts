/* tslint:disable */
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiConfiguration, ApiConfigurationInterface } from './api-configuration';

import { CustomCartEndpointService } from './services/custom-cart-endpoint.service';
import { CheckoutEndpointService } from './services/checkout-endpoint.service';
import { CartEndpointService } from './services/cart-endpoint.service';
import { CatalogEndpointService } from './services/catalog-endpoint.service';
import { CustomerEndpointService } from './services/customer-endpoint.service';
import { CustomCustomerEndpointService } from './services/custom-customer-endpoint.service';
import { BasicErrorControllerService } from './services/basic-error-controller.service';
import { OrderHistoryEndpointService } from './services/order-history-endpoint.service';
import { PreviewTemplateControllerService } from './services/preview-template-controller.service';
import { PromotionMessageEndpointService } from './services/promotion-message-endpoint.service';
import { RatingEndpointService } from './services/rating-endpoint.service';
import { RelatedProductsEndpointService } from './services/related-products-endpoint.service';
import { FulfillmentEndpointService } from './services/fulfillment-endpoint.service';
import { WishlistEndpointService } from './services/wishlist-endpoint.service';

/**
 * Provider for all Api services, plus ApiConfiguration
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiConfiguration,
    CustomCartEndpointService,
    CheckoutEndpointService,
    CartEndpointService,
    CatalogEndpointService,
    CustomerEndpointService,
    CustomCustomerEndpointService,
    BasicErrorControllerService,
    OrderHistoryEndpointService,
    PreviewTemplateControllerService,
    PromotionMessageEndpointService,
    RatingEndpointService,
    RelatedProductsEndpointService,
    FulfillmentEndpointService,
    WishlistEndpointService
  ],
})
export class ApiModule {
  static forRoot(customParams: ApiConfigurationInterface): ModuleWithProviders {
    return {
      ngModule: ApiModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: {rootUrl: customParams.rootUrl}
        }
      ]
    }
  }
}
