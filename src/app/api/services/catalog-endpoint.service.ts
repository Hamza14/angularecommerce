/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CategoriesWrapper } from '../models/categories-wrapper';
import { CategoryWrapper } from '../models/category-wrapper';
import { CategoryAttributeWrapper } from '../models/category-attribute-wrapper';
import { MediaWrapper } from '../models/media-wrapper';
import { ProductWrapper } from '../models/product-wrapper';
import { ProductAttributeWrapper } from '../models/product-attribute-wrapper';
import { RelatedProductWrapper } from '../models/related-product-wrapper';
import { SkuWrapper } from '../models/sku-wrapper';
import { SearchResultsWrapper } from '../models/search-results-wrapper';
import { InventoryWrapper } from '../models/inventory-wrapper';
import { SkuAttributeWrapper } from '../models/sku-attribute-wrapper';

/**
 * Catalog Endpoint
 */
@Injectable({
  providedIn: 'root',
})
class CatalogEndpointService extends __BaseService {
  static readonly findAllCategoriesUsingGET1Path = '/catalog/categories';
  static readonly findCategoryByIdOrNameUsingGET1Path = '/catalog/category';
  static readonly findCategoryByIdUsingGET1Path = '/catalog/category/{categoryId}';
  static readonly findActiveSubCategoriesUsingGET1Path = '/catalog/category/{categoryId}/activeSubcategories';
  static readonly findCategoryAttributesForCategoryUsingGET1Path = '/catalog/category/{categoryId}/attributes';
  static readonly findSubCategoriesUsingGET1Path = '/catalog/category/{categoryId}/categories';
  static readonly findMediaForCategoryUsingGET1Path = '/catalog/category/{id}/media';
  static readonly findProductByIdUsingGET1Path = '/catalog/product/{id}';
  static readonly findProductAttributesForProductUsingGET1Path = '/catalog/product/{productId}/attributes';
  static readonly findParentCategoriesForProductUsingGET1Path = '/catalog/product/{productId}/categories';
  static readonly findCrossSaleProductsByProductUsingGET1Path = '/catalog/product/{productId}/crosssale';
  static readonly findDefaultSkuByProductIdUsingGET1Path = '/catalog/product/{productId}/defaultSku';
  static readonly findMediaForProductUsingGET1Path = '/catalog/product/{productId}/media';
  static readonly findSkusByProductByIdUsingGET1Path = '/catalog/product/{productId}/skus';
  static readonly findUpSaleProductsByProductUsingGET1Path = '/catalog/product/{productId}/upsale';
  static readonly findSearchResultsByQueryUsingGET1Path = '/catalog/search';
  static readonly findSearchResultsByCategoryAndQueryUsingGET1Path = '/catalog/search/category/{categoryId}';
  static readonly findInventoryForSkusUsingGET1Path = '/catalog/sku/inventory';
  static readonly findSkuByIdUsingGET1Path = '/catalog/sku/{skuId}';
  static readonly findSkuAttributesForSkuUsingGET1Path = '/catalog/sku/{skuId}/attributes';
  static readonly findMediaForSkuUsingGET1Path = '/catalog/sku/{skuId}/media';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `CatalogEndpointService.FindAllCategoriesUsingGET1Params` containing the following parameters:
   *
   * - `offset`: offset
   *
   * - `name`: name
   *
   * - `limit`: limit
   *
   * @return OK
   */
  findAllCategoriesUsingGET1Response(params: CatalogEndpointService.FindAllCategoriesUsingGET1Params): __Observable<__StrictHttpResponse<CategoriesWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.name != null) __params = __params.set('name', params.name.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/categories`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CategoriesWrapper>;
      })
    );
  }
  /**
   * @param params The `CatalogEndpointService.FindAllCategoriesUsingGET1Params` containing the following parameters:
   *
   * - `offset`: offset
   *
   * - `name`: name
   *
   * - `limit`: limit
   *
   * @return OK
   */
  findAllCategoriesUsingGET1(params: CatalogEndpointService.FindAllCategoriesUsingGET1Params): __Observable<CategoriesWrapper> {
    return this.findAllCategoriesUsingGET1Response(params).pipe(
      __map(_r => _r.body as CategoriesWrapper)
    );
  }

  /**
   * @param params The `CatalogEndpointService.FindCategoryByIdOrNameUsingGET1Params` containing the following parameters:
   *
   * - `searchParameter`: searchParameter
   *
   * - `subcategoryOffset`: subcategoryOffset
   *
   * - `subcategoryLimit`: subcategoryLimit
   *
   * - `productOffset`: productOffset
   *
   * - `productLimit`: productLimit
   *
   * @return OK
   */
  findCategoryByIdOrNameUsingGET1Response(params: CatalogEndpointService.FindCategoryByIdOrNameUsingGET1Params): __Observable<__StrictHttpResponse<CategoryWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.searchParameter != null) __params = __params.set('searchParameter', params.searchParameter.toString());
    if (params.subcategoryOffset != null) __params = __params.set('subcategoryOffset', params.subcategoryOffset.toString());
    if (params.subcategoryLimit != null) __params = __params.set('subcategoryLimit', params.subcategoryLimit.toString());
    if (params.productOffset != null) __params = __params.set('productOffset', params.productOffset.toString());
    if (params.productLimit != null) __params = __params.set('productLimit', params.productLimit.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/category`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CategoryWrapper>;
      })
    );
  }
  /**
   * @param params The `CatalogEndpointService.FindCategoryByIdOrNameUsingGET1Params` containing the following parameters:
   *
   * - `searchParameter`: searchParameter
   *
   * - `subcategoryOffset`: subcategoryOffset
   *
   * - `subcategoryLimit`: subcategoryLimit
   *
   * - `productOffset`: productOffset
   *
   * - `productLimit`: productLimit
   *
   * @return OK
   */
  findCategoryByIdOrNameUsingGET1(params: CatalogEndpointService.FindCategoryByIdOrNameUsingGET1Params): __Observable<CategoryWrapper> {
    return this.findCategoryByIdOrNameUsingGET1Response(params).pipe(
      __map(_r => _r.body as CategoryWrapper)
    );
  }

  /**
   * @param params The `CatalogEndpointService.FindCategoryByIdUsingGET1Params` containing the following parameters:
   *
   * - `categoryId`: categoryId
   *
   * - `subcategoryOffset`: subcategoryOffset
   *
   * - `subcategoryLimit`: subcategoryLimit
   *
   * - `productOffset`: productOffset
   *
   * - `productLimit`: productLimit
   *
   * @return OK
   */
  findCategoryByIdUsingGET1Response(params: CatalogEndpointService.FindCategoryByIdUsingGET1Params): __Observable<__StrictHttpResponse<CategoryWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.subcategoryOffset != null) __params = __params.set('subcategoryOffset', params.subcategoryOffset.toString());
    if (params.subcategoryLimit != null) __params = __params.set('subcategoryLimit', params.subcategoryLimit.toString());
    if (params.productOffset != null) __params = __params.set('productOffset', params.productOffset.toString());
    if (params.productLimit != null) __params = __params.set('productLimit', params.productLimit.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/category/${params.categoryId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CategoryWrapper>;
      })
    );
  }
  /**
   * @param params The `CatalogEndpointService.FindCategoryByIdUsingGET1Params` containing the following parameters:
   *
   * - `categoryId`: categoryId
   *
   * - `subcategoryOffset`: subcategoryOffset
   *
   * - `subcategoryLimit`: subcategoryLimit
   *
   * - `productOffset`: productOffset
   *
   * - `productLimit`: productLimit
   *
   * @return OK
   */
  findCategoryByIdUsingGET1(params: CatalogEndpointService.FindCategoryByIdUsingGET1Params): __Observable<CategoryWrapper> {
    return this.findCategoryByIdUsingGET1Response(params).pipe(
      __map(_r => _r.body as CategoryWrapper)
    );
  }

  /**
   * @param params The `CatalogEndpointService.FindActiveSubCategoriesUsingGET1Params` containing the following parameters:
   *
   * - `categoryId`: categoryId
   *
   * - `offset`: offset
   *
   * - `limit`: limit
   *
   * @return OK
   */
  findActiveSubCategoriesUsingGET1Response(params: CatalogEndpointService.FindActiveSubCategoriesUsingGET1Params): __Observable<__StrictHttpResponse<CategoriesWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/category/${params.categoryId}/activeSubcategories`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CategoriesWrapper>;
      })
    );
  }
  /**
   * @param params The `CatalogEndpointService.FindActiveSubCategoriesUsingGET1Params` containing the following parameters:
   *
   * - `categoryId`: categoryId
   *
   * - `offset`: offset
   *
   * - `limit`: limit
   *
   * @return OK
   */
  findActiveSubCategoriesUsingGET1(params: CatalogEndpointService.FindActiveSubCategoriesUsingGET1Params): __Observable<CategoriesWrapper> {
    return this.findActiveSubCategoriesUsingGET1Response(params).pipe(
      __map(_r => _r.body as CategoriesWrapper)
    );
  }

  /**
   * @param categoryId categoryId
   * @return OK
   */
  findCategoryAttributesForCategoryUsingGET1Response(categoryId: number): __Observable<__StrictHttpResponse<Array<CategoryAttributeWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/category/${categoryId}/attributes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<CategoryAttributeWrapper>>;
      })
    );
  }
  /**
   * @param categoryId categoryId
   * @return OK
   */
  findCategoryAttributesForCategoryUsingGET1(categoryId: number): __Observable<Array<CategoryAttributeWrapper>> {
    return this.findCategoryAttributesForCategoryUsingGET1Response(categoryId).pipe(
      __map(_r => _r.body as Array<CategoryAttributeWrapper>)
    );
  }

  /**
   * @param params The `CatalogEndpointService.FindSubCategoriesUsingGET1Params` containing the following parameters:
   *
   * - `categoryId`: categoryId
   *
   * - `offset`: offset
   *
   * - `limit`: limit
   *
   * - `active`: active
   *
   * @return OK
   */
  findSubCategoriesUsingGET1Response(params: CatalogEndpointService.FindSubCategoriesUsingGET1Params): __Observable<__StrictHttpResponse<CategoriesWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    if (params.active != null) __params = __params.set('active', params.active.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/category/${params.categoryId}/categories`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CategoriesWrapper>;
      })
    );
  }
  /**
   * @param params The `CatalogEndpointService.FindSubCategoriesUsingGET1Params` containing the following parameters:
   *
   * - `categoryId`: categoryId
   *
   * - `offset`: offset
   *
   * - `limit`: limit
   *
   * - `active`: active
   *
   * @return OK
   */
  findSubCategoriesUsingGET1(params: CatalogEndpointService.FindSubCategoriesUsingGET1Params): __Observable<CategoriesWrapper> {
    return this.findSubCategoriesUsingGET1Response(params).pipe(
      __map(_r => _r.body as CategoriesWrapper)
    );
  }

  /**
   * @param id id
   * @return OK
   */
  findMediaForCategoryUsingGET1Response(id: number): __Observable<__StrictHttpResponse<Array<MediaWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/category/${id}/media`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<MediaWrapper>>;
      })
    );
  }
  /**
   * @param id id
   * @return OK
   */
  findMediaForCategoryUsingGET1(id: number): __Observable<Array<MediaWrapper>> {
    return this.findMediaForCategoryUsingGET1Response(id).pipe(
      __map(_r => _r.body as Array<MediaWrapper>)
    );
  }

  /**
   * @param params The `CatalogEndpointService.FindProductByIdUsingGET1Params` containing the following parameters:
   *
   * - `id`: id
   *
   * - `includePromotionMessages`: includePromotionMessages
   *
   * - `includePriceData`: includePriceData
   *
   * @return OK
   */
  findProductByIdUsingGET1Response(params: CatalogEndpointService.FindProductByIdUsingGET1Params): __Observable<__StrictHttpResponse<ProductWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.includePromotionMessages != null) __params = __params.set('includePromotionMessages', params.includePromotionMessages.toString());
    if (params.includePriceData != null) __params = __params.set('includePriceData', params.includePriceData.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/product/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ProductWrapper>;
      })
    );
  }
  /**
   * @param params The `CatalogEndpointService.FindProductByIdUsingGET1Params` containing the following parameters:
   *
   * - `id`: id
   *
   * - `includePromotionMessages`: includePromotionMessages
   *
   * - `includePriceData`: includePriceData
   *
   * @return OK
   */
  findProductByIdUsingGET1(params: CatalogEndpointService.FindProductByIdUsingGET1Params): __Observable<ProductWrapper> {
    return this.findProductByIdUsingGET1Response(params).pipe(
      __map(_r => _r.body as ProductWrapper)
    );
  }

  /**
   * @param productId productId
   * @return OK
   */
  findProductAttributesForProductUsingGET1Response(productId: number): __Observable<__StrictHttpResponse<Array<ProductAttributeWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/product/${productId}/attributes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<ProductAttributeWrapper>>;
      })
    );
  }
  /**
   * @param productId productId
   * @return OK
   */
  findProductAttributesForProductUsingGET1(productId: number): __Observable<Array<ProductAttributeWrapper>> {
    return this.findProductAttributesForProductUsingGET1Response(productId).pipe(
      __map(_r => _r.body as Array<ProductAttributeWrapper>)
    );
  }

  /**
   * @param productId productId
   * @return OK
   */
  findParentCategoriesForProductUsingGET1Response(productId: number): __Observable<__StrictHttpResponse<CategoriesWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/product/${productId}/categories`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CategoriesWrapper>;
      })
    );
  }
  /**
   * @param productId productId
   * @return OK
   */
  findParentCategoriesForProductUsingGET1(productId: number): __Observable<CategoriesWrapper> {
    return this.findParentCategoriesForProductUsingGET1Response(productId).pipe(
      __map(_r => _r.body as CategoriesWrapper)
    );
  }

  /**
   * @param params The `CatalogEndpointService.FindCrossSaleProductsByProductUsingGET1Params` containing the following parameters:
   *
   * - `productId`: productId
   *
   * - `offset`: offset
   *
   * - `limit`: limit
   *
   * @return OK
   */
  findCrossSaleProductsByProductUsingGET1Response(params: CatalogEndpointService.FindCrossSaleProductsByProductUsingGET1Params): __Observable<__StrictHttpResponse<Array<RelatedProductWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/product/${params.productId}/crosssale`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<RelatedProductWrapper>>;
      })
    );
  }
  /**
   * @param params The `CatalogEndpointService.FindCrossSaleProductsByProductUsingGET1Params` containing the following parameters:
   *
   * - `productId`: productId
   *
   * - `offset`: offset
   *
   * - `limit`: limit
   *
   * @return OK
   */
  findCrossSaleProductsByProductUsingGET1(params: CatalogEndpointService.FindCrossSaleProductsByProductUsingGET1Params): __Observable<Array<RelatedProductWrapper>> {
    return this.findCrossSaleProductsByProductUsingGET1Response(params).pipe(
      __map(_r => _r.body as Array<RelatedProductWrapper>)
    );
  }

  /**
   * @param productId productId
   * @return OK
   */
  findDefaultSkuByProductIdUsingGET1Response(productId: number): __Observable<__StrictHttpResponse<SkuWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/product/${productId}/defaultSku`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SkuWrapper>;
      })
    );
  }
  /**
   * @param productId productId
   * @return OK
   */
  findDefaultSkuByProductIdUsingGET1(productId: number): __Observable<SkuWrapper> {
    return this.findDefaultSkuByProductIdUsingGET1Response(productId).pipe(
      __map(_r => _r.body as SkuWrapper)
    );
  }

  /**
   * @param productId productId
   * @return OK
   */
  findMediaForProductUsingGET1Response(productId: number): __Observable<__StrictHttpResponse<Array<MediaWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/product/${productId}/media`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<MediaWrapper>>;
      })
    );
  }
  /**
   * @param productId productId
   * @return OK
   */
  findMediaForProductUsingGET1(productId: number): __Observable<Array<MediaWrapper>> {
    return this.findMediaForProductUsingGET1Response(productId).pipe(
      __map(_r => _r.body as Array<MediaWrapper>)
    );
  }

  /**
   * @param productId productId
   * @return OK
   */
  findSkusByProductByIdUsingGET1Response(productId: number): __Observable<__StrictHttpResponse<Array<SkuWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/product/${productId}/skus`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<SkuWrapper>>;
      })
    );
  }
  /**
   * @param productId productId
   * @return OK
   */
  findSkusByProductByIdUsingGET1(productId: number): __Observable<Array<SkuWrapper>> {
    return this.findSkusByProductByIdUsingGET1Response(productId).pipe(
      __map(_r => _r.body as Array<SkuWrapper>)
    );
  }

  /**
   * @param params The `CatalogEndpointService.FindUpSaleProductsByProductUsingGET1Params` containing the following parameters:
   *
   * - `productId`: productId
   *
   * - `offset`: offset
   *
   * - `limit`: limit
   *
   * @return OK
   */
  findUpSaleProductsByProductUsingGET1Response(params: CatalogEndpointService.FindUpSaleProductsByProductUsingGET1Params): __Observable<__StrictHttpResponse<Array<RelatedProductWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.offset != null) __params = __params.set('offset', params.offset.toString());
    if (params.limit != null) __params = __params.set('limit', params.limit.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/product/${params.productId}/upsale`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<RelatedProductWrapper>>;
      })
    );
  }
  /**
   * @param params The `CatalogEndpointService.FindUpSaleProductsByProductUsingGET1Params` containing the following parameters:
   *
   * - `productId`: productId
   *
   * - `offset`: offset
   *
   * - `limit`: limit
   *
   * @return OK
   */
  findUpSaleProductsByProductUsingGET1(params: CatalogEndpointService.FindUpSaleProductsByProductUsingGET1Params): __Observable<Array<RelatedProductWrapper>> {
    return this.findUpSaleProductsByProductUsingGET1Response(params).pipe(
      __map(_r => _r.body as Array<RelatedProductWrapper>)
    );
  }

  /**
   * @param params The `CatalogEndpointService.FindSearchResultsByQueryUsingGET1Params` containing the following parameters:
   *
   * - `q`: q
   *
   * - `pageSize`: pageSize
   *
   * - `page`: page
   *
   * - `includePromotionMessages`: includePromotionMessages
   *
   * - `includePriceData`: includePriceData
   *
   * @return OK
   */
  findSearchResultsByQueryUsingGET1Response(params: CatalogEndpointService.FindSearchResultsByQueryUsingGET1Params): __Observable<__StrictHttpResponse<SearchResultsWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.q != null) __params = __params.set('q', params.q.toString());
    if (params.pageSize != null) __params = __params.set('pageSize', params.pageSize.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    if (params.includePromotionMessages != null) __params = __params.set('includePromotionMessages', params.includePromotionMessages.toString());
    if (params.includePriceData != null) __params = __params.set('includePriceData', params.includePriceData.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/search`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SearchResultsWrapper>;
      })
    );
  }
  /**
   * @param params The `CatalogEndpointService.FindSearchResultsByQueryUsingGET1Params` containing the following parameters:
   *
   * - `q`: q
   *
   * - `pageSize`: pageSize
   *
   * - `page`: page
   *
   * - `includePromotionMessages`: includePromotionMessages
   *
   * - `includePriceData`: includePriceData
   *
   * @return OK
   */
  findSearchResultsByQueryUsingGET1(params: CatalogEndpointService.FindSearchResultsByQueryUsingGET1Params): __Observable<SearchResultsWrapper> {
    return this.findSearchResultsByQueryUsingGET1Response(params).pipe(
      __map(_r => _r.body as SearchResultsWrapper)
    );
  }

  /**
   * @param params The `CatalogEndpointService.FindSearchResultsByCategoryAndQueryUsingGET1Params` containing the following parameters:
   *
   * - `categoryId`: categoryId
   *
   * - `q`: q
   *
   * - `pageSize`: pageSize
   *
   * - `page`: page
   *
   * - `includePromotionMessages`: includePromotionMessages
   *
   * - `includePriceData`: includePriceData
   *
   * @return OK
   */
  findSearchResultsByCategoryAndQueryUsingGET1Response(params: CatalogEndpointService.FindSearchResultsByCategoryAndQueryUsingGET1Params): __Observable<__StrictHttpResponse<SearchResultsWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.q != null) __params = __params.set('q', params.q.toString());
    if (params.pageSize != null) __params = __params.set('pageSize', params.pageSize.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    if (params.includePromotionMessages != null) __params = __params.set('includePromotionMessages', params.includePromotionMessages.toString());
    if (params.includePriceData != null) __params = __params.set('includePriceData', params.includePriceData.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/search/category/${params.categoryId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SearchResultsWrapper>;
      })
    );
  }
  /**
   * @param params The `CatalogEndpointService.FindSearchResultsByCategoryAndQueryUsingGET1Params` containing the following parameters:
   *
   * - `categoryId`: categoryId
   *
   * - `q`: q
   *
   * - `pageSize`: pageSize
   *
   * - `page`: page
   *
   * - `includePromotionMessages`: includePromotionMessages
   *
   * - `includePriceData`: includePriceData
   *
   * @return OK
   */
  findSearchResultsByCategoryAndQueryUsingGET1(params: CatalogEndpointService.FindSearchResultsByCategoryAndQueryUsingGET1Params): __Observable<SearchResultsWrapper> {
    return this.findSearchResultsByCategoryAndQueryUsingGET1Response(params).pipe(
      __map(_r => _r.body as SearchResultsWrapper)
    );
  }

  /**
   * @param id id
   * @return OK
   */
  findInventoryForSkusUsingGET1Response(id: Array<number>): __Observable<__StrictHttpResponse<Array<InventoryWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    (id || []).forEach(val => {if (val != null) __params = __params.append('id', val.toString())});
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/sku/inventory`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<InventoryWrapper>>;
      })
    );
  }
  /**
   * @param id id
   * @return OK
   */
  findInventoryForSkusUsingGET1(id: Array<number>): __Observable<Array<InventoryWrapper>> {
    return this.findInventoryForSkusUsingGET1Response(id).pipe(
      __map(_r => _r.body as Array<InventoryWrapper>)
    );
  }

  /**
   * @param skuId skuId
   * @return OK
   */
  findSkuByIdUsingGET1Response(skuId: number): __Observable<__StrictHttpResponse<SkuWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/sku/${skuId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SkuWrapper>;
      })
    );
  }
  /**
   * @param skuId skuId
   * @return OK
   */
  findSkuByIdUsingGET1(skuId: number): __Observable<SkuWrapper> {
    return this.findSkuByIdUsingGET1Response(skuId).pipe(
      __map(_r => _r.body as SkuWrapper)
    );
  }

  /**
   * @param skuId skuId
   * @return OK
   */
  findSkuAttributesForSkuUsingGET1Response(skuId: number): __Observable<__StrictHttpResponse<Array<SkuAttributeWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/sku/${skuId}/attributes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<SkuAttributeWrapper>>;
      })
    );
  }
  /**
   * @param skuId skuId
   * @return OK
   */
  findSkuAttributesForSkuUsingGET1(skuId: number): __Observable<Array<SkuAttributeWrapper>> {
    return this.findSkuAttributesForSkuUsingGET1Response(skuId).pipe(
      __map(_r => _r.body as Array<SkuAttributeWrapper>)
    );
  }

  /**
   * @param skuId skuId
   * @return OK
   */
  findMediaForSkuUsingGET1Response(skuId: number): __Observable<__StrictHttpResponse<Array<MediaWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/catalog/sku/${skuId}/media`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<MediaWrapper>>;
      })
    );
  }
  /**
   * @param skuId skuId
   * @return OK
   */
  findMediaForSkuUsingGET1(skuId: number): __Observable<Array<MediaWrapper>> {
    return this.findMediaForSkuUsingGET1Response(skuId).pipe(
      __map(_r => _r.body as Array<MediaWrapper>)
    );
  }
}

module CatalogEndpointService {

  /**
   * Parameters for findAllCategoriesUsingGET1
   */
  export interface FindAllCategoriesUsingGET1Params {

    /**
     * offset
     */
    offset?: number;

    /**
     * name
     */
    name?: string;

    /**
     * limit
     */
    limit?: number;
  }

  /**
   * Parameters for findCategoryByIdOrNameUsingGET1
   */
  export interface FindCategoryByIdOrNameUsingGET1Params {

    /**
     * searchParameter
     */
    searchParameter: string;

    /**
     * subcategoryOffset
     */
    subcategoryOffset?: number;

    /**
     * subcategoryLimit
     */
    subcategoryLimit?: number;

    /**
     * productOffset
     */
    productOffset?: number;

    /**
     * productLimit
     */
    productLimit?: number;
  }

  /**
   * Parameters for findCategoryByIdUsingGET1
   */
  export interface FindCategoryByIdUsingGET1Params {

    /**
     * categoryId
     */
    categoryId: number;

    /**
     * subcategoryOffset
     */
    subcategoryOffset?: number;

    /**
     * subcategoryLimit
     */
    subcategoryLimit?: number;

    /**
     * productOffset
     */
    productOffset?: number;

    /**
     * productLimit
     */
    productLimit?: number;
  }

  /**
   * Parameters for findActiveSubCategoriesUsingGET1
   */
  export interface FindActiveSubCategoriesUsingGET1Params {

    /**
     * categoryId
     */
    categoryId: number;

    /**
     * offset
     */
    offset?: number;

    /**
     * limit
     */
    limit?: number;
  }

  /**
   * Parameters for findSubCategoriesUsingGET1
   */
  export interface FindSubCategoriesUsingGET1Params {

    /**
     * categoryId
     */
    categoryId: number;

    /**
     * offset
     */
    offset?: number;

    /**
     * limit
     */
    limit?: number;

    /**
     * active
     */
    active?: boolean;
  }

  /**
   * Parameters for findProductByIdUsingGET1
   */
  export interface FindProductByIdUsingGET1Params {

    /**
     * id
     */
    id: number;

    /**
     * includePromotionMessages
     */
    includePromotionMessages?: boolean;

    /**
     * includePriceData
     */
    includePriceData?: boolean;
  }

  /**
   * Parameters for findCrossSaleProductsByProductUsingGET1
   */
  export interface FindCrossSaleProductsByProductUsingGET1Params {

    /**
     * productId
     */
    productId: number;

    /**
     * offset
     */
    offset?: number;

    /**
     * limit
     */
    limit?: number;
  }

  /**
   * Parameters for findUpSaleProductsByProductUsingGET1
   */
  export interface FindUpSaleProductsByProductUsingGET1Params {

    /**
     * productId
     */
    productId: number;

    /**
     * offset
     */
    offset?: number;

    /**
     * limit
     */
    limit?: number;
  }

  /**
   * Parameters for findSearchResultsByQueryUsingGET1
   */
  export interface FindSearchResultsByQueryUsingGET1Params {

    /**
     * q
     */
    q: string;

    /**
     * pageSize
     */
    pageSize?: number;

    /**
     * page
     */
    page?: number;

    /**
     * includePromotionMessages
     */
    includePromotionMessages?: boolean;

    /**
     * includePriceData
     */
    includePriceData?: boolean;
  }

  /**
   * Parameters for findSearchResultsByCategoryAndQueryUsingGET1
   */
  export interface FindSearchResultsByCategoryAndQueryUsingGET1Params {

    /**
     * categoryId
     */
    categoryId: number;

    /**
     * q
     */
    q?: string;

    /**
     * pageSize
     */
    pageSize?: number;

    /**
     * page
     */
    page?: number;

    /**
     * includePromotionMessages
     */
    includePromotionMessages?: boolean;

    /**
     * includePriceData
     */
    includePriceData?: boolean;
  }
}

export { CatalogEndpointService }
