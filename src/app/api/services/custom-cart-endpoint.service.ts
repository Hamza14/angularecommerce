/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import {Observable, Observable as __Observable, Subject} from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { OrderWrapper } from '../models/order-wrapper';
import { OrderItemWrapper } from '../models/order-item-wrapper';

/**
 * Custom Cart Endpoint
 */
@Injectable({
  providedIn: 'root',
})
class CustomCartEndpointService extends __BaseService {
  static readonly findCartForCustomerUsingGET1Path = '/cart';
  static readonly createNewCartForCustomerUsingPOST3Path = '/cart';
  static readonly addItemToOrderUsingPOST3Path = '/cart/{cartId}/item';
  private storageSub= new Subject<boolean>();
  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }
  watchCart(): Observable<any> {
    return this.storageSub.asObservable();
  }
  /**
   * @param customerId undefined
   * @return OK
   */
  findCartForCustomerUsingGET1Response(customerId: number): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (customerId != null) __params = __params.set('customerId', customerId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cart`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param customerId undefined
   * @return OK
   */
  findCartForCustomerUsingGET1(customerId: number): __Observable<OrderWrapper> {
    return this.findCartForCustomerUsingGET1Response(customerId).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param customerId customerId
   * @return OK
   */
  createNewCartForCustomerUsingPOST3Response(customerId: number): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (customerId != null) __params = __params.set('customerId', customerId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cart`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param customerId customerId
   * @return OK
   */
  createNewCartForCustomerUsingPOST3(customerId: number): __Observable<OrderWrapper> {
    return this.createNewCartForCustomerUsingPOST3Response(customerId).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CustomCartEndpointService.AddItemToOrderUsingPOST3Params` containing the following parameters:
   *
   * - `orderItemWrapper`: orderItemWrapper
   *
   * - `customerId`: customerId
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  addItemToOrderUsingPOST3Response(params: CustomCartEndpointService.AddItemToOrderUsingPOST3Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.orderItemWrapper;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cart/${params.cartId}/item`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        this.storageSub.next();
        return _r as __StrictHttpResponse<OrderWrapper>;

      })
    );
  }
  /**
   * @param params The `CustomCartEndpointService.AddItemToOrderUsingPOST3Params` containing the following parameters:
   *
   * - `orderItemWrapper`: orderItemWrapper
   *
   * - `customerId`: customerId
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  addItemToOrderUsingPOST3(params: CustomCartEndpointService.AddItemToOrderUsingPOST3Params): __Observable<OrderWrapper> {
    return this.addItemToOrderUsingPOST3Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)


    );

  }
}

module CustomCartEndpointService {

  /**
   * Parameters for addItemToOrderUsingPOST3
   */
  export interface AddItemToOrderUsingPOST3Params {

    /**
     * orderItemWrapper
     */
    orderItemWrapper: OrderItemWrapper;

    /**
     * customerId
     */
    customerId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }
}

export { CustomCartEndpointService }
