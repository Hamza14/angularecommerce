/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { OrderWrapper } from '../models/order-wrapper';

/**
 * Order History Endpoint
 */
@Injectable({
  providedIn: 'root',
})
class OrderHistoryEndpointService extends __BaseService {
  static readonly findOrdersForCustomerUsingGET1Path = '/orders';
  static readonly findAllOrdersForCustomerUsingGET1Path = '/orders/summary';
  static readonly findOrderByIdUsingGET1Path = '/orders/{orderId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `OrderHistoryEndpointService.FindOrdersForCustomerUsingGET1Params` containing the following parameters:
   *
   * - `orderStatus`: orderStatus
   *
   * - `orderNumber`: orderNumber
   *
   * @return OK
   */
  findOrdersForCustomerUsingGET1Response(params: OrderHistoryEndpointService.FindOrdersForCustomerUsingGET1Params): __Observable<__StrictHttpResponse<Array<OrderWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.orderStatus != null) __params = __params.set('orderStatus', params.orderStatus.toString());
    if (params.orderNumber != null) __params = __params.set('orderNumber', params.orderNumber.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/orders`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<OrderWrapper>>;
      })
    );
  }
  /**
   * @param params The `OrderHistoryEndpointService.FindOrdersForCustomerUsingGET1Params` containing the following parameters:
   *
   * - `orderStatus`: orderStatus
   *
   * - `orderNumber`: orderNumber
   *
   * @return OK
   */
  findOrdersForCustomerUsingGET1(params: OrderHistoryEndpointService.FindOrdersForCustomerUsingGET1Params): __Observable<Array<OrderWrapper>> {
    return this.findOrdersForCustomerUsingGET1Response(params).pipe(
      __map(_r => _r.body as Array<OrderWrapper>)
    );
  }

  /**
   * @return OK
   */
  findAllOrdersForCustomerUsingGET1Response(): __Observable<__StrictHttpResponse<Array<OrderWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/orders/summary`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<OrderWrapper>>;
      })
    );
  }
  /**
   * @return OK
   */
  findAllOrdersForCustomerUsingGET1(): __Observable<Array<OrderWrapper>> {
    return this.findAllOrdersForCustomerUsingGET1Response().pipe(
      __map(_r => _r.body as Array<OrderWrapper>)
    );
  }

  /**
   * @param orderId orderId
   * @return OK
   */
  findOrderByIdUsingGET1Response(orderId: number): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/orders/${orderId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param orderId orderId
   * @return OK
   */
  findOrderByIdUsingGET1(orderId: number): __Observable<OrderWrapper> {
    return this.findOrderByIdUsingGET1Response(orderId).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }
}

module OrderHistoryEndpointService {

  /**
   * Parameters for findOrdersForCustomerUsingGET1
   */
  export interface FindOrdersForCustomerUsingGET1Params {

    /**
     * orderStatus
     */
    orderStatus?: string;

    /**
     * orderNumber
     */
    orderNumber?: string;
  }
}

export { OrderHistoryEndpointService }
