/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ProductWrapper } from '../models/product-wrapper';

/**
 * Related Products Endpoint
 */
@Injectable({
  providedIn: 'root',
})
class RelatedProductsEndpointService extends __BaseService {
  static readonly getRelatedProductsUsingGET1Path = '/related-products';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `RelatedProductsEndpointService.GetRelatedProductsUsingGET1Params` containing the following parameters:
   *
   * - `type`: type
   *
   * - `quantity`: quantity
   *
   * - `productKey`: productKey
   *
   * - `productId`: productId
   *
   * - `includePromotionMessages`: includePromotionMessages
   *
   * - `includePriceData`: includePriceData
   *
   * - `categoryKey`: categoryKey
   *
   * - `categoryId`: categoryId
   *
   * @return OK
   */
  getRelatedProductsUsingGET1Response(params: RelatedProductsEndpointService.GetRelatedProductsUsingGET1Params): __Observable<__StrictHttpResponse<Array<ProductWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.type != null) __params = __params.set('type', params.type.toString());
    if (params.quantity != null) __params = __params.set('quantity', params.quantity.toString());
    if (params.productKey != null) __params = __params.set('productKey', params.productKey.toString());
    if (params.productId != null) __params = __params.set('productId', params.productId.toString());
    if (params.includePromotionMessages != null) __params = __params.set('includePromotionMessages', params.includePromotionMessages.toString());
    if (params.includePriceData != null) __params = __params.set('includePriceData', params.includePriceData.toString());
    if (params.categoryKey != null) __params = __params.set('categoryKey', params.categoryKey.toString());
    if (params.categoryId != null) __params = __params.set('categoryId', params.categoryId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/related-products`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<ProductWrapper>>;
      })
    );
  }
  /**
   * @param params The `RelatedProductsEndpointService.GetRelatedProductsUsingGET1Params` containing the following parameters:
   *
   * - `type`: type
   *
   * - `quantity`: quantity
   *
   * - `productKey`: productKey
   *
   * - `productId`: productId
   *
   * - `includePromotionMessages`: includePromotionMessages
   *
   * - `includePriceData`: includePriceData
   *
   * - `categoryKey`: categoryKey
   *
   * - `categoryId`: categoryId
   *
   * @return OK
   */
  getRelatedProductsUsingGET1(params: RelatedProductsEndpointService.GetRelatedProductsUsingGET1Params): __Observable<Array<ProductWrapper>> {
    return this.getRelatedProductsUsingGET1Response(params).pipe(
      __map(_r => _r.body as Array<ProductWrapper>)
    );
  }
}

module RelatedProductsEndpointService {

  /**
   * Parameters for getRelatedProductsUsingGET1
   */
  export interface GetRelatedProductsUsingGET1Params {

    /**
     * type
     */
    type?: string;

    /**
     * quantity
     */
    quantity?: number;

    /**
     * productKey
     */
    productKey?: string;

    /**
     * productId
     */
    productId?: number;

    /**
     * includePromotionMessages
     */
    includePromotionMessages?: boolean;

    /**
     * includePriceData
     */
    includePriceData?: boolean;

    /**
     * categoryKey
     */
    categoryKey?: string;

    /**
     * categoryId
     */
    categoryId?: number;
  }
}

export { RelatedProductsEndpointService }
