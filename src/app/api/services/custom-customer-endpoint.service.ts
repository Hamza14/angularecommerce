/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CustomerWrapper } from '../models/customer-wrapper';

/**
 * Custom Customer Endpoint
 */
@Injectable({
  providedIn: 'root',
})
class CustomCustomerEndpointService extends __BaseService {
  static readonly addCustomerUsingPOST1Path = '/customer/addCustomer';
  static readonly findCustomerByEmailUsingGET1Path = '/customer/getCustomer';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `CustomCustomerEndpointService.AddCustomerUsingPOST1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `password`: password
   *
   * @return OK
   */
  addCustomerUsingPOST1Response(params: CustomCustomerEndpointService.AddCustomerUsingPOST1Params): __Observable<__StrictHttpResponse<CustomerWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;
    if (params.password != null) __params = __params.set('password', params.password.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/customer/addCustomer`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerWrapper>;
      })
    );
  }
  /**
   * @param params The `CustomCustomerEndpointService.AddCustomerUsingPOST1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `password`: password
   *
   * @return OK
   */
  addCustomerUsingPOST1(params: CustomCustomerEndpointService.AddCustomerUsingPOST1Params): __Observable<CustomerWrapper> {
    return this.addCustomerUsingPOST1Response(params).pipe(
      __map(_r => _r.body as CustomerWrapper)
    );
  }

  /**
   * @param params The `CustomCustomerEndpointService.FindCustomerByEmailUsingGET1Params` containing the following parameters:
   *
   * - `password`: password
   *
   * - `email`: email
   *
   * @return OK
   */
  findCustomerByEmailUsingGET1Response(params: CustomCustomerEndpointService.FindCustomerByEmailUsingGET1Params): __Observable<__StrictHttpResponse<CustomerWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.password != null) __params = __params.set('password', params.password.toString());
    if (params.email != null) __params = __params.set('email', params.email.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/customer/getCustomer`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerWrapper>;
      })
    );
  }
  /**
   * @param params The `CustomCustomerEndpointService.FindCustomerByEmailUsingGET1Params` containing the following parameters:
   *
   * - `password`: password
   *
   * - `email`: email
   *
   * @return OK
   */
  findCustomerByEmailUsingGET1(params: CustomCustomerEndpointService.FindCustomerByEmailUsingGET1Params): __Observable<CustomerWrapper> {
    return this.findCustomerByEmailUsingGET1Response(params).pipe(
      __map(_r => _r.body as CustomerWrapper)
    );
  }
}

module CustomCustomerEndpointService {

  /**
   * Parameters for addCustomerUsingPOST1
   */
  export interface AddCustomerUsingPOST1Params {

    /**
     * wrapper
     */
    wrapper: CustomerWrapper;

    /**
     * password
     */
    password: string;
  }

  /**
   * Parameters for findCustomerByEmailUsingGET1
   */
  export interface FindCustomerByEmailUsingGET1Params {

    /**
     * password
     */
    password: string;

    /**
     * email
     */
    email: string;
  }
}

export { CustomCustomerEndpointService }
