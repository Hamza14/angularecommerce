/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { OrderWrapper } from '../models/order-wrapper';
import { OrderAttributeWrapper } from '../models/order-attribute-wrapper';
import { OrderItemAttributeWrapper } from '../models/order-item-attribute-wrapper';

/**
 * Cart Endpoint
 */
@Injectable({
  providedIn: 'root',
})
class CartEndpointService extends __BaseService {
  static readonly findCartByIdUsingGET1Path = '/cart/{cartId}';
  static readonly updateOrderAttributesUsingPUT1Path = '/cart/{cartId}/attributes';
  static readonly deleteOrderAttributesUsingDELETE1Path = '/cart/{cartId}/attributes';
  static readonly updateProductOptionsUsingPUT1Path = '/cart/{cartId}/item/{itemId}/attributes';
  static readonly deleteProductOptionsUsingDELETE1Path = '/cart/{cartId}/item/{itemId}/attributes';
  static readonly updateItemQuantityUsingPUT1Path = '/cart/{cartId}/items/{itemId}';
  static readonly removeItemFromOrderUsingDELETE1Path = '/cart/{cartId}/items/{itemId}';
  static readonly addOfferCodeUsingPOST1Path = '/cart/{cartId}/offer/{promoCode}';
  static readonly removeOfferCodeUsingDELETE1Path = '/cart/{cartId}/offer/{promoCode}';
  static readonly removeAllOfferCodesUsingDELETE1Path = '/cart/{cartId}/offers';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `CartEndpointService.FindCartByIdUsingGET1Params` containing the following parameters:
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  findCartByIdUsingGET1Response(params: CartEndpointService.FindCartByIdUsingGET1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cart/${params.cartId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `CartEndpointService.FindCartByIdUsingGET1Params` containing the following parameters:
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  findCartByIdUsingGET1(params: CartEndpointService.FindCartByIdUsingGET1Params): __Observable<OrderWrapper> {
    return this.findCartByIdUsingGET1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CartEndpointService.UpdateOrderAttributesUsingPUT1Params` containing the following parameters:
   *
   * - `requestParams`: requestParams
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  updateOrderAttributesUsingPUT1Response(params: CartEndpointService.UpdateOrderAttributesUsingPUT1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.requestParams;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cart/${params.cartId}/attributes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `CartEndpointService.UpdateOrderAttributesUsingPUT1Params` containing the following parameters:
   *
   * - `requestParams`: requestParams
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  updateOrderAttributesUsingPUT1(params: CartEndpointService.UpdateOrderAttributesUsingPUT1Params): __Observable<OrderWrapper> {
    return this.updateOrderAttributesUsingPUT1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CartEndpointService.DeleteOrderAttributesUsingDELETE1Params` containing the following parameters:
   *
   * - `requestParams`: requestParams
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  deleteOrderAttributesUsingDELETE1Response(params: CartEndpointService.DeleteOrderAttributesUsingDELETE1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.requestParams;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cart/${params.cartId}/attributes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `CartEndpointService.DeleteOrderAttributesUsingDELETE1Params` containing the following parameters:
   *
   * - `requestParams`: requestParams
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  deleteOrderAttributesUsingDELETE1(params: CartEndpointService.DeleteOrderAttributesUsingDELETE1Params): __Observable<OrderWrapper> {
    return this.deleteOrderAttributesUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CartEndpointService.UpdateProductOptionsUsingPUT1Params` containing the following parameters:
   *
   * - `requestParams`: requestParams
   *
   * - `itemId`: itemId
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  updateProductOptionsUsingPUT1Response(params: CartEndpointService.UpdateProductOptionsUsingPUT1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.requestParams;

    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cart/${params.cartId}/item/${params.itemId}/attributes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `CartEndpointService.UpdateProductOptionsUsingPUT1Params` containing the following parameters:
   *
   * - `requestParams`: requestParams
   *
   * - `itemId`: itemId
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  updateProductOptionsUsingPUT1(params: CartEndpointService.UpdateProductOptionsUsingPUT1Params): __Observable<OrderWrapper> {
    return this.updateProductOptionsUsingPUT1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CartEndpointService.DeleteProductOptionsUsingDELETE1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `requestParams`: requestParams
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  deleteProductOptionsUsingDELETE1Response(params: CartEndpointService.DeleteProductOptionsUsingDELETE1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    __body = params.requestParams;
    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cart/${params.cartId}/item/${params.itemId}/attributes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `CartEndpointService.DeleteProductOptionsUsingDELETE1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `requestParams`: requestParams
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  deleteProductOptionsUsingDELETE1(params: CartEndpointService.DeleteProductOptionsUsingDELETE1Params): __Observable<OrderWrapper> {
    return this.deleteProductOptionsUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CartEndpointService.UpdateItemQuantityUsingPUT1Params` containing the following parameters:
   *
   * - `quantity`: quantity
   *
   * - `itemId`: itemId
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  updateItemQuantityUsingPUT1Response(params: CartEndpointService.UpdateItemQuantityUsingPUT1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.quantity != null) __params = __params.set('quantity', params.quantity.toString());

    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cart/${params.cartId}/items/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `CartEndpointService.UpdateItemQuantityUsingPUT1Params` containing the following parameters:
   *
   * - `quantity`: quantity
   *
   * - `itemId`: itemId
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  updateItemQuantityUsingPUT1(params: CartEndpointService.UpdateItemQuantityUsingPUT1Params): __Observable<OrderWrapper> {
    return this.updateItemQuantityUsingPUT1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CartEndpointService.RemoveItemFromOrderUsingDELETE1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  removeItemFromOrderUsingDELETE1Response(params: CartEndpointService.RemoveItemFromOrderUsingDELETE1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cart/${params.cartId}/items/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `CartEndpointService.RemoveItemFromOrderUsingDELETE1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  removeItemFromOrderUsingDELETE1(params: CartEndpointService.RemoveItemFromOrderUsingDELETE1Params): __Observable<OrderWrapper> {
    return this.removeItemFromOrderUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CartEndpointService.AddOfferCodeUsingPOST1Params` containing the following parameters:
   *
   * - `promoCode`: promoCode
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  addOfferCodeUsingPOST1Response(params: CartEndpointService.AddOfferCodeUsingPOST1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cart/${params.cartId}/offer/${params.promoCode}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `CartEndpointService.AddOfferCodeUsingPOST1Params` containing the following parameters:
   *
   * - `promoCode`: promoCode
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  addOfferCodeUsingPOST1(params: CartEndpointService.AddOfferCodeUsingPOST1Params): __Observable<OrderWrapper> {
    return this.addOfferCodeUsingPOST1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CartEndpointService.RemoveOfferCodeUsingDELETE1Params` containing the following parameters:
   *
   * - `promoCode`: promoCode
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  removeOfferCodeUsingDELETE1Response(params: CartEndpointService.RemoveOfferCodeUsingDELETE1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cart/${params.cartId}/offer/${params.promoCode}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `CartEndpointService.RemoveOfferCodeUsingDELETE1Params` containing the following parameters:
   *
   * - `promoCode`: promoCode
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  removeOfferCodeUsingDELETE1(params: CartEndpointService.RemoveOfferCodeUsingDELETE1Params): __Observable<OrderWrapper> {
    return this.removeOfferCodeUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CartEndpointService.RemoveAllOfferCodesUsingDELETE1Params` containing the following parameters:
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  removeAllOfferCodesUsingDELETE1Response(params: CartEndpointService.RemoveAllOfferCodesUsingDELETE1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cart/${params.cartId}/offers`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `CartEndpointService.RemoveAllOfferCodesUsingDELETE1Params` containing the following parameters:
   *
   * - `customerId`:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  removeAllOfferCodesUsingDELETE1(params: CartEndpointService.RemoveAllOfferCodesUsingDELETE1Params): __Observable<OrderWrapper> {
    return this.removeAllOfferCodesUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }
}

module CartEndpointService {

  /**
   * Parameters for findCartByIdUsingGET1
   */
  export interface FindCartByIdUsingGET1Params {
    customerId: number;

    /**
     * cartId
     */
    cartId: number;
  }

  /**
   * Parameters for updateOrderAttributesUsingPUT1
   */
  export interface UpdateOrderAttributesUsingPUT1Params {

    /**
     * requestParams
     */
    requestParams: Array<OrderAttributeWrapper>;
    customerId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for deleteOrderAttributesUsingDELETE1
   */
  export interface DeleteOrderAttributesUsingDELETE1Params {

    /**
     * requestParams
     */
    requestParams: Array<OrderAttributeWrapper>;
    customerId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for updateProductOptionsUsingPUT1
   */
  export interface UpdateProductOptionsUsingPUT1Params {

    /**
     * requestParams
     */
    requestParams: Array<OrderItemAttributeWrapper>;

    /**
     * itemId
     */
    itemId: number;
    customerId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for deleteProductOptionsUsingDELETE1
   */
  export interface DeleteProductOptionsUsingDELETE1Params {

    /**
     * itemId
     */
    itemId: number;
    customerId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * requestParams
     */
    requestParams?: Array<OrderItemAttributeWrapper>;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for updateItemQuantityUsingPUT1
   */
  export interface UpdateItemQuantityUsingPUT1Params {

    /**
     * quantity
     */
    quantity: number;

    /**
     * itemId
     */
    itemId: number;
    customerId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for removeItemFromOrderUsingDELETE1
   */
  export interface RemoveItemFromOrderUsingDELETE1Params {

    /**
     * itemId
     */
    itemId: number;
    customerId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for addOfferCodeUsingPOST1
   */
  export interface AddOfferCodeUsingPOST1Params {

    /**
     * promoCode
     */
    promoCode: string;
    customerId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for removeOfferCodeUsingDELETE1
   */
  export interface RemoveOfferCodeUsingDELETE1Params {

    /**
     * promoCode
     */
    promoCode: string;
    customerId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for removeAllOfferCodesUsingDELETE1
   */
  export interface RemoveAllOfferCodesUsingDELETE1Params {
    customerId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }
}

export { CartEndpointService }
