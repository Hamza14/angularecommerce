/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CustomerWrapper } from '../models/customer-wrapper';
import { CustomerAddressWrapper } from '../models/customer-address-wrapper';
import { CustomerAttributeWrapper } from '../models/customer-attribute-wrapper';
import { ChangePasswordForm } from '../models/change-password-form';
import { CustomerPaymentWrapper } from '../models/customer-payment-wrapper';

/**
 * Customer Endpoint
 */
@Injectable({
  providedIn: 'root',
})
class CustomerEndpointService extends __BaseService {
  static readonly findCustomerByEmailUsingGET3Path = '/customer';
  static readonly addCustomerUsingPOST3Path = '/customer';
  static readonly updateCustomerUsingPUT1Path = '/customer';
  static readonly findAddressUsingGET1Path = '/customer/address';
  static readonly addAddressUsingPUT1Path = '/customer/address';
  static readonly updateAddressUsingPUT1Path = '/customer/address/{addressId}';
  static readonly removeAddressUsingDELETE1Path = '/customer/address/{addressName}';
  static readonly findAllAddressesUsingGET1Path = '/customer/addresses';
  static readonly removeAllAddressesUsingDELETE1Path = '/customer/addresses';
  static readonly addAttributeUsingPUT1Path = '/customer/attribute';
  static readonly removeAttributeUsingDELETE1Path = '/customer/attribute/{attributeName}';
  static readonly removeAllAttributesUsingDELETE1Path = '/customer/attributes';
  static readonly changePasswordUsingPOST1Path = '/customer/password';
  static readonly findCustomerPaymentUsingGET1Path = '/customer/payment';
  static readonly addCustomerPaymentUsingPOST1Path = '/customer/payment';
  static readonly updateCustomerPaymentUsingPUT1Path = '/customer/payment/{paymentId}';
  static readonly removeCustomerPaymentUsingDELETE1Path = '/customer/payment/{paymentId}';
  static readonly findAllCustomerPaymentsUsingGET1Path = '/customer/payments';
  static readonly removeAllCustomerPaymentsUsingDELETE1Path = '/customer/payments';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param email email
   * @return OK
   */
  findCustomerByEmailUsingGET3Response(email: string): __Observable<__StrictHttpResponse<CustomerWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (email != null) __params = __params.set('email', email.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/customer`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerWrapper>;
      })
    );
  }
  /**
   * @param email email
   * @return OK
   */
  findCustomerByEmailUsingGET3(email: string): __Observable<CustomerWrapper> {
    return this.findCustomerByEmailUsingGET3Response(email).pipe(
      __map(_r => _r.body as CustomerWrapper)
    );
  }

  /**
   * @param wrapper wrapper
   * @return OK
   */
  addCustomerUsingPOST3Response(wrapper: CustomerWrapper): __Observable<__StrictHttpResponse<CustomerWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = wrapper;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/customer`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerWrapper>;
      })
    );
  }
  /**
   * @param wrapper wrapper
   * @return OK
   */
  addCustomerUsingPOST3(wrapper: CustomerWrapper): __Observable<CustomerWrapper> {
    return this.addCustomerUsingPOST3Response(wrapper).pipe(
      __map(_r => _r.body as CustomerWrapper)
    );
  }

  /**
   * @param params The `CustomerEndpointService.UpdateCustomerUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `customerId`:
   *
   * @return OK
   */
  updateCustomerUsingPUT1Response(params: CustomerEndpointService.UpdateCustomerUsingPUT1Params): __Observable<__StrictHttpResponse<CustomerWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/customer`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerWrapper>;
      })
    );
  }
  /**
   * @param params The `CustomerEndpointService.UpdateCustomerUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `customerId`:
   *
   * @return OK
   */
  updateCustomerUsingPUT1(params: CustomerEndpointService.UpdateCustomerUsingPUT1Params): __Observable<CustomerWrapper> {
    return this.updateCustomerUsingPUT1Response(params).pipe(
      __map(_r => _r.body as CustomerWrapper)
    );
  }

  /**
   * @param params The `CustomerEndpointService.FindAddressUsingGET1Params` containing the following parameters:
   *
   * - `customerId`:
   *
   * - `addressName`: addressName
   *
   * @return OK
   */
  findAddressUsingGET1Response(params: CustomerEndpointService.FindAddressUsingGET1Params): __Observable<__StrictHttpResponse<CustomerAddressWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());
    if (params.addressName != null) __params = __params.set('addressName', params.addressName.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/customer/address`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerAddressWrapper>;
      })
    );
  }
  /**
   * @param params The `CustomerEndpointService.FindAddressUsingGET1Params` containing the following parameters:
   *
   * - `customerId`:
   *
   * - `addressName`: addressName
   *
   * @return OK
   */
  findAddressUsingGET1(params: CustomerEndpointService.FindAddressUsingGET1Params): __Observable<CustomerAddressWrapper> {
    return this.findAddressUsingGET1Response(params).pipe(
      __map(_r => _r.body as CustomerAddressWrapper)
    );
  }

  /**
   * @param params The `CustomerEndpointService.AddAddressUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `customerId`:
   *
   * @return OK
   */
  addAddressUsingPUT1Response(params: CustomerEndpointService.AddAddressUsingPUT1Params): __Observable<__StrictHttpResponse<CustomerAddressWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/customer/address`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerAddressWrapper>;
      })
    );
  }
  /**
   * @param params The `CustomerEndpointService.AddAddressUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `customerId`:
   *
   * @return OK
   */
  addAddressUsingPUT1(params: CustomerEndpointService.AddAddressUsingPUT1Params): __Observable<CustomerAddressWrapper> {
    return this.addAddressUsingPUT1Response(params).pipe(
      __map(_r => _r.body as CustomerAddressWrapper)
    );
  }

  /**
   * @param params The `CustomerEndpointService.UpdateAddressUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `customerId`:
   *
   * - `addressId`: addressId
   *
   * @return OK
   */
  updateAddressUsingPUT1Response(params: CustomerEndpointService.UpdateAddressUsingPUT1Params): __Observable<__StrictHttpResponse<CustomerAddressWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/customer/address/${params.addressId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerAddressWrapper>;
      })
    );
  }
  /**
   * @param params The `CustomerEndpointService.UpdateAddressUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `customerId`:
   *
   * - `addressId`: addressId
   *
   * @return OK
   */
  updateAddressUsingPUT1(params: CustomerEndpointService.UpdateAddressUsingPUT1Params): __Observable<CustomerAddressWrapper> {
    return this.updateAddressUsingPUT1Response(params).pipe(
      __map(_r => _r.body as CustomerAddressWrapper)
    );
  }

  /**
   * @param params The `CustomerEndpointService.RemoveAddressUsingDELETE1Params` containing the following parameters:
   *
   * - `customerId`:
   *
   * - `addressName`: addressName
   *
   * @return OK
   */
  removeAddressUsingDELETE1Response(params: CustomerEndpointService.RemoveAddressUsingDELETE1Params): __Observable<__StrictHttpResponse<Array<CustomerAddressWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/customer/address/${params.addressName}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<CustomerAddressWrapper>>;
      })
    );
  }
  /**
   * @param params The `CustomerEndpointService.RemoveAddressUsingDELETE1Params` containing the following parameters:
   *
   * - `customerId`:
   *
   * - `addressName`: addressName
   *
   * @return OK
   */
  removeAddressUsingDELETE1(params: CustomerEndpointService.RemoveAddressUsingDELETE1Params): __Observable<Array<CustomerAddressWrapper>> {
    return this.removeAddressUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as Array<CustomerAddressWrapper>)
    );
  }

  /**
   * @param customerId undefined
   * @return OK
   */
  findAllAddressesUsingGET1Response(customerId: number): __Observable<__StrictHttpResponse<Array<CustomerAddressWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (customerId != null) __params = __params.set('customerId', customerId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/customer/addresses`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<CustomerAddressWrapper>>;
      })
    );
  }
  /**
   * @param customerId undefined
   * @return OK
   */
  findAllAddressesUsingGET1(customerId: number): __Observable<Array<CustomerAddressWrapper>> {
    return this.findAllAddressesUsingGET1Response(customerId).pipe(
      __map(_r => _r.body as Array<CustomerAddressWrapper>)
    );
  }

  /**
   * @param customerId undefined
   * @return OK
   */
  removeAllAddressesUsingDELETE1Response(customerId: number): __Observable<__StrictHttpResponse<CustomerWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (customerId != null) __params = __params.set('customerId', customerId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/customer/addresses`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerWrapper>;
      })
    );
  }
  /**
   * @param customerId undefined
   * @return OK
   */
  removeAllAddressesUsingDELETE1(customerId: number): __Observable<CustomerWrapper> {
    return this.removeAllAddressesUsingDELETE1Response(customerId).pipe(
      __map(_r => _r.body as CustomerWrapper)
    );
  }

  /**
   * @param params The `CustomerEndpointService.AddAttributeUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `customerId`:
   *
   * @return OK
   */
  addAttributeUsingPUT1Response(params: CustomerEndpointService.AddAttributeUsingPUT1Params): __Observable<__StrictHttpResponse<CustomerWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/customer/attribute`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerWrapper>;
      })
    );
  }
  /**
   * @param params The `CustomerEndpointService.AddAttributeUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `customerId`:
   *
   * @return OK
   */
  addAttributeUsingPUT1(params: CustomerEndpointService.AddAttributeUsingPUT1Params): __Observable<CustomerWrapper> {
    return this.addAttributeUsingPUT1Response(params).pipe(
      __map(_r => _r.body as CustomerWrapper)
    );
  }

  /**
   * @param params The `CustomerEndpointService.RemoveAttributeUsingDELETE1Params` containing the following parameters:
   *
   * - `customerId`:
   *
   * - `attributeName`: attributeName
   *
   * @return OK
   */
  removeAttributeUsingDELETE1Response(params: CustomerEndpointService.RemoveAttributeUsingDELETE1Params): __Observable<__StrictHttpResponse<CustomerWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/customer/attribute/${params.attributeName}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerWrapper>;
      })
    );
  }
  /**
   * @param params The `CustomerEndpointService.RemoveAttributeUsingDELETE1Params` containing the following parameters:
   *
   * - `customerId`:
   *
   * - `attributeName`: attributeName
   *
   * @return OK
   */
  removeAttributeUsingDELETE1(params: CustomerEndpointService.RemoveAttributeUsingDELETE1Params): __Observable<CustomerWrapper> {
    return this.removeAttributeUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as CustomerWrapper)
    );
  }

  /**
   * @param customerId undefined
   * @return OK
   */
  removeAllAttributesUsingDELETE1Response(customerId: number): __Observable<__StrictHttpResponse<CustomerWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (customerId != null) __params = __params.set('customerId', customerId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/customer/attributes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerWrapper>;
      })
    );
  }
  /**
   * @param customerId undefined
   * @return OK
   */
  removeAllAttributesUsingDELETE1(customerId: number): __Observable<CustomerWrapper> {
    return this.removeAllAttributesUsingDELETE1Response(customerId).pipe(
      __map(_r => _r.body as CustomerWrapper)
    );
  }

  /**
   * @param changePasswordForm changePasswordForm
   * @return OK
   */
  changePasswordUsingPOST1Response(changePasswordForm: ChangePasswordForm): __Observable<__StrictHttpResponse<CustomerWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = changePasswordForm;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/customer/password`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerWrapper>;
      })
    );
  }
  /**
   * @param changePasswordForm changePasswordForm
   * @return OK
   */
  changePasswordUsingPOST1(changePasswordForm: ChangePasswordForm): __Observable<CustomerWrapper> {
    return this.changePasswordUsingPOST1Response(changePasswordForm).pipe(
      __map(_r => _r.body as CustomerWrapper)
    );
  }

  /**
   * @param params The `CustomerEndpointService.FindCustomerPaymentUsingGET1Params` containing the following parameters:
   *
   * - `paymentId`: paymentId
   *
   * - `customerId`:
   *
   * @return OK
   */
  findCustomerPaymentUsingGET1Response(params: CustomerEndpointService.FindCustomerPaymentUsingGET1Params): __Observable<__StrictHttpResponse<CustomerPaymentWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.paymentId != null) __params = __params.set('paymentId', params.paymentId.toString());
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/customer/payment`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerPaymentWrapper>;
      })
    );
  }
  /**
   * @param params The `CustomerEndpointService.FindCustomerPaymentUsingGET1Params` containing the following parameters:
   *
   * - `paymentId`: paymentId
   *
   * - `customerId`:
   *
   * @return OK
   */
  findCustomerPaymentUsingGET1(params: CustomerEndpointService.FindCustomerPaymentUsingGET1Params): __Observable<CustomerPaymentWrapper> {
    return this.findCustomerPaymentUsingGET1Response(params).pipe(
      __map(_r => _r.body as CustomerPaymentWrapper)
    );
  }

  /**
   * @param params The `CustomerEndpointService.AddCustomerPaymentUsingPOST1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `customerId`:
   *
   * @return OK
   */
  addCustomerPaymentUsingPOST1Response(params: CustomerEndpointService.AddCustomerPaymentUsingPOST1Params): __Observable<__StrictHttpResponse<CustomerPaymentWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;
    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/customer/payment`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerPaymentWrapper>;
      })
    );
  }
  /**
   * @param params The `CustomerEndpointService.AddCustomerPaymentUsingPOST1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `customerId`:
   *
   * @return OK
   */
  addCustomerPaymentUsingPOST1(params: CustomerEndpointService.AddCustomerPaymentUsingPOST1Params): __Observable<CustomerPaymentWrapper> {
    return this.addCustomerPaymentUsingPOST1Response(params).pipe(
      __map(_r => _r.body as CustomerPaymentWrapper)
    );
  }

  /**
   * @param params The `CustomerEndpointService.UpdateCustomerPaymentUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `paymentId`: paymentId
   *
   * - `customerId`:
   *
   * @return OK
   */
  updateCustomerPaymentUsingPUT1Response(params: CustomerEndpointService.UpdateCustomerPaymentUsingPUT1Params): __Observable<__StrictHttpResponse<CustomerPaymentWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;

    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/customer/payment/${params.paymentId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerPaymentWrapper>;
      })
    );
  }
  /**
   * @param params The `CustomerEndpointService.UpdateCustomerPaymentUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `paymentId`: paymentId
   *
   * - `customerId`:
   *
   * @return OK
   */
  updateCustomerPaymentUsingPUT1(params: CustomerEndpointService.UpdateCustomerPaymentUsingPUT1Params): __Observable<CustomerPaymentWrapper> {
    return this.updateCustomerPaymentUsingPUT1Response(params).pipe(
      __map(_r => _r.body as CustomerPaymentWrapper)
    );
  }

  /**
   * @param params The `CustomerEndpointService.RemoveCustomerPaymentUsingDELETE1Params` containing the following parameters:
   *
   * - `paymentId`: paymentId
   *
   * - `customerId`:
   *
   * @return OK
   */
  removeCustomerPaymentUsingDELETE1Response(params: CustomerEndpointService.RemoveCustomerPaymentUsingDELETE1Params): __Observable<__StrictHttpResponse<Array<CustomerPaymentWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.customerId != null) __params = __params.set('customerId', params.customerId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/customer/payment/${params.paymentId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<CustomerPaymentWrapper>>;
      })
    );
  }
  /**
   * @param params The `CustomerEndpointService.RemoveCustomerPaymentUsingDELETE1Params` containing the following parameters:
   *
   * - `paymentId`: paymentId
   *
   * - `customerId`:
   *
   * @return OK
   */
  removeCustomerPaymentUsingDELETE1(params: CustomerEndpointService.RemoveCustomerPaymentUsingDELETE1Params): __Observable<Array<CustomerPaymentWrapper>> {
    return this.removeCustomerPaymentUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as Array<CustomerPaymentWrapper>)
    );
  }

  /**
   * @param customerId undefined
   * @return OK
   */
  findAllCustomerPaymentsUsingGET1Response(customerId: number): __Observable<__StrictHttpResponse<Array<CustomerPaymentWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (customerId != null) __params = __params.set('customerId', customerId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/customer/payments`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<CustomerPaymentWrapper>>;
      })
    );
  }
  /**
   * @param customerId undefined
   * @return OK
   */
  findAllCustomerPaymentsUsingGET1(customerId: number): __Observable<Array<CustomerPaymentWrapper>> {
    return this.findAllCustomerPaymentsUsingGET1Response(customerId).pipe(
      __map(_r => _r.body as Array<CustomerPaymentWrapper>)
    );
  }

  /**
   * @param customerId undefined
   * @return OK
   */
  removeAllCustomerPaymentsUsingDELETE1Response(customerId: number): __Observable<__StrictHttpResponse<CustomerWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (customerId != null) __params = __params.set('customerId', customerId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/customer/payments`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CustomerWrapper>;
      })
    );
  }
  /**
   * @param customerId undefined
   * @return OK
   */
  removeAllCustomerPaymentsUsingDELETE1(customerId: number): __Observable<CustomerWrapper> {
    return this.removeAllCustomerPaymentsUsingDELETE1Response(customerId).pipe(
      __map(_r => _r.body as CustomerWrapper)
    );
  }
}

module CustomerEndpointService {

  /**
   * Parameters for updateCustomerUsingPUT1
   */
  export interface UpdateCustomerUsingPUT1Params {

    /**
     * wrapper
     */
    wrapper: CustomerWrapper;
    customerId: number;
  }

  /**
   * Parameters for findAddressUsingGET1
   */
  export interface FindAddressUsingGET1Params {
    customerId: number;

    /**
     * addressName
     */
    addressName: string;
  }

  /**
   * Parameters for addAddressUsingPUT1
   */
  export interface AddAddressUsingPUT1Params {

    /**
     * wrapper
     */
    wrapper: CustomerAddressWrapper;
    customerId: number;
  }

  /**
   * Parameters for updateAddressUsingPUT1
   */
  export interface UpdateAddressUsingPUT1Params {

    /**
     * wrapper
     */
    wrapper: CustomerAddressWrapper;
    customerId: number;

    /**
     * addressId
     */
    addressId: number;
  }

  /**
   * Parameters for removeAddressUsingDELETE1
   */
  export interface RemoveAddressUsingDELETE1Params {
    customerId: number;

    /**
     * addressName
     */
    addressName: string;
  }

  /**
   * Parameters for addAttributeUsingPUT1
   */
  export interface AddAttributeUsingPUT1Params {

    /**
     * wrapper
     */
    wrapper: CustomerAttributeWrapper;
    customerId: number;
  }

  /**
   * Parameters for removeAttributeUsingDELETE1
   */
  export interface RemoveAttributeUsingDELETE1Params {
    customerId: number;

    /**
     * attributeName
     */
    attributeName: string;
  }

  /**
   * Parameters for findCustomerPaymentUsingGET1
   */
  export interface FindCustomerPaymentUsingGET1Params {

    /**
     * paymentId
     */
    paymentId: number;
    customerId: number;
  }

  /**
   * Parameters for addCustomerPaymentUsingPOST1
   */
  export interface AddCustomerPaymentUsingPOST1Params {

    /**
     * wrapper
     */
    wrapper: CustomerPaymentWrapper;
    customerId: number;
  }

  /**
   * Parameters for updateCustomerPaymentUsingPUT1
   */
  export interface UpdateCustomerPaymentUsingPUT1Params {

    /**
     * wrapper
     */
    wrapper: CustomerPaymentWrapper;

    /**
     * paymentId
     */
    paymentId: number;
    customerId: number;
  }

  /**
   * Parameters for removeCustomerPaymentUsingDELETE1
   */
  export interface RemoveCustomerPaymentUsingDELETE1Params {

    /**
     * paymentId
     */
    paymentId: number;
    customerId: number;
  }
}

export { CustomerEndpointService }
