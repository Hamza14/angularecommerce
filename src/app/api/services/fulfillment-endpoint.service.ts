/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { FulfillmentOptionWrapper } from '../models/fulfillment-option-wrapper';
import { FulfillmentEstimationWrapper } from '../models/fulfillment-estimation-wrapper';
import { FulfillmentGroupWrapper } from '../models/fulfillment-group-wrapper';
import { FulfillmentGroupItemWrapper } from '../models/fulfillment-group-item-wrapper';
import { OrderWrapper } from '../models/order-wrapper';
import { AddressWrapper } from '../models/address-wrapper';

/**
 * Fulfillment Endpoint
 */
@Injectable({
  providedIn: 'root',
})
class FulfillmentEndpointService extends __BaseService {
  static readonly findFulfillmentOptionsUsingGET1Path = '/shipping/options';
  static readonly getFulfillmentEstimationsUsingGET1Path = '/shipping/{cartId}/estimate';
  static readonly getFulfillmentEstimationsUsingHEAD1Path = '/shipping/{cartId}/estimate';
  static readonly getFulfillmentEstimationsUsingPOST1Path = '/shipping/{cartId}/estimate';
  static readonly getFulfillmentEstimationsUsingPUT1Path = '/shipping/{cartId}/estimate';
  static readonly getFulfillmentEstimationsUsingDELETE1Path = '/shipping/{cartId}/estimate';
  static readonly getFulfillmentEstimationsUsingOPTIONS1Path = '/shipping/{cartId}/estimate';
  static readonly getFulfillmentEstimationsUsingPATCH1Path = '/shipping/{cartId}/estimate';
  static readonly addFulfillmentGroupToOrderUsingPOST1Path = '/shipping/{cartId}/group';
  static readonly removeFulfillmentGroupFromOrderUsingDELETE1Path = '/shipping/{cartId}/group/{fulfillmentGroupId}';
  static readonly updateFulfillmentGroupUsingPATCH1Path = '/shipping/{cartId}/group/{fulfillmentGroupId}';
  static readonly addItemToFulfillmentGroupUsingPOST1Path = '/shipping/{cartId}/group/{fulfillmentGroupId}/item';
  static readonly removeOrderItemFromFulfillmentGroupUsingDELETE1Path = '/shipping/{cartId}/group/{fulfillmentGroupId}/item/{itemId}';
  static readonly addFulfillmentOptionToFulfillmentGroupUsingPUT1Path = '/shipping/{cartId}/group/{fulfillmentGroupId}/option/{fulfillmentOptionId}';
  static readonly findFulfillmentGroupsForOrderUsingGET1Path = '/shipping/{cartId}/groups';
  static readonly removeAllFulfillmentGroupsFromOrderUsingDELETE1Path = '/shipping/{cartId}/groups';
  static readonly updateFulfillmentGroupAddressUsingPUT1Path = '/shipping/{cartId}/{fulfillmentGroupId}/address';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param fulfillmentType fulfillmentType
   * @return OK
   */
  findFulfillmentOptionsUsingGET1Response(fulfillmentType: string): __Observable<__StrictHttpResponse<Array<FulfillmentOptionWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (fulfillmentType != null) __params = __params.set('fulfillmentType', fulfillmentType.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/shipping/options`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<FulfillmentOptionWrapper>>;
      })
    );
  }
  /**
   * @param fulfillmentType fulfillmentType
   * @return OK
   */
  findFulfillmentOptionsUsingGET1(fulfillmentType: string): __Observable<Array<FulfillmentOptionWrapper>> {
    return this.findFulfillmentOptionsUsingGET1Response(fulfillmentType).pipe(
      __map(_r => _r.body as Array<FulfillmentOptionWrapper>)
    );
  }

  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingGET1Response(cartId: number): __Observable<__StrictHttpResponse<Array<FulfillmentEstimationWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/shipping/${cartId}/estimate`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<FulfillmentEstimationWrapper>>;
      })
    );
  }
  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingGET1(cartId: number): __Observable<Array<FulfillmentEstimationWrapper>> {
    return this.getFulfillmentEstimationsUsingGET1Response(cartId).pipe(
      __map(_r => _r.body as Array<FulfillmentEstimationWrapper>)
    );
  }

  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingHEAD1Response(cartId: number): __Observable<__StrictHttpResponse<Array<FulfillmentEstimationWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'HEAD',
      this.rootUrl + `/shipping/${cartId}/estimate`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<FulfillmentEstimationWrapper>>;
      })
    );
  }
  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingHEAD1(cartId: number): __Observable<Array<FulfillmentEstimationWrapper>> {
    return this.getFulfillmentEstimationsUsingHEAD1Response(cartId).pipe(
      __map(_r => _r.body as Array<FulfillmentEstimationWrapper>)
    );
  }

  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingPOST1Response(cartId: number): __Observable<__StrictHttpResponse<Array<FulfillmentEstimationWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/shipping/${cartId}/estimate`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<FulfillmentEstimationWrapper>>;
      })
    );
  }
  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingPOST1(cartId: number): __Observable<Array<FulfillmentEstimationWrapper>> {
    return this.getFulfillmentEstimationsUsingPOST1Response(cartId).pipe(
      __map(_r => _r.body as Array<FulfillmentEstimationWrapper>)
    );
  }

  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingPUT1Response(cartId: number): __Observable<__StrictHttpResponse<Array<FulfillmentEstimationWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/shipping/${cartId}/estimate`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<FulfillmentEstimationWrapper>>;
      })
    );
  }
  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingPUT1(cartId: number): __Observable<Array<FulfillmentEstimationWrapper>> {
    return this.getFulfillmentEstimationsUsingPUT1Response(cartId).pipe(
      __map(_r => _r.body as Array<FulfillmentEstimationWrapper>)
    );
  }

  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingDELETE1Response(cartId: number): __Observable<__StrictHttpResponse<Array<FulfillmentEstimationWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/shipping/${cartId}/estimate`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<FulfillmentEstimationWrapper>>;
      })
    );
  }
  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingDELETE1(cartId: number): __Observable<Array<FulfillmentEstimationWrapper>> {
    return this.getFulfillmentEstimationsUsingDELETE1Response(cartId).pipe(
      __map(_r => _r.body as Array<FulfillmentEstimationWrapper>)
    );
  }

  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingOPTIONS1Response(cartId: number): __Observable<__StrictHttpResponse<Array<FulfillmentEstimationWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'OPTIONS',
      this.rootUrl + `/shipping/${cartId}/estimate`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<FulfillmentEstimationWrapper>>;
      })
    );
  }
  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingOPTIONS1(cartId: number): __Observable<Array<FulfillmentEstimationWrapper>> {
    return this.getFulfillmentEstimationsUsingOPTIONS1Response(cartId).pipe(
      __map(_r => _r.body as Array<FulfillmentEstimationWrapper>)
    );
  }

  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingPATCH1Response(cartId: number): __Observable<__StrictHttpResponse<Array<FulfillmentEstimationWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'PATCH',
      this.rootUrl + `/shipping/${cartId}/estimate`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<FulfillmentEstimationWrapper>>;
      })
    );
  }
  /**
   * @param cartId cartId
   * @return OK
   */
  getFulfillmentEstimationsUsingPATCH1(cartId: number): __Observable<Array<FulfillmentEstimationWrapper>> {
    return this.getFulfillmentEstimationsUsingPATCH1Response(cartId).pipe(
      __map(_r => _r.body as Array<FulfillmentEstimationWrapper>)
    );
  }

  /**
   * @param params The `FulfillmentEndpointService.AddFulfillmentGroupToOrderUsingPOST1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  addFulfillmentGroupToOrderUsingPOST1Response(params: FulfillmentEndpointService.AddFulfillmentGroupToOrderUsingPOST1Params): __Observable<__StrictHttpResponse<FulfillmentGroupWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;

    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/shipping/${params.cartId}/group`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FulfillmentGroupWrapper>;
      })
    );
  }
  /**
   * @param params The `FulfillmentEndpointService.AddFulfillmentGroupToOrderUsingPOST1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  addFulfillmentGroupToOrderUsingPOST1(params: FulfillmentEndpointService.AddFulfillmentGroupToOrderUsingPOST1Params): __Observable<FulfillmentGroupWrapper> {
    return this.addFulfillmentGroupToOrderUsingPOST1Response(params).pipe(
      __map(_r => _r.body as FulfillmentGroupWrapper)
    );
  }

  /**
   * @param params The `FulfillmentEndpointService.RemoveFulfillmentGroupFromOrderUsingDELETE1Params` containing the following parameters:
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  removeFulfillmentGroupFromOrderUsingDELETE1Response(params: FulfillmentEndpointService.RemoveFulfillmentGroupFromOrderUsingDELETE1Params): __Observable<__StrictHttpResponse<Array<FulfillmentGroupWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/shipping/${params.cartId}/group/${params.fulfillmentGroupId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<FulfillmentGroupWrapper>>;
      })
    );
  }
  /**
   * @param params The `FulfillmentEndpointService.RemoveFulfillmentGroupFromOrderUsingDELETE1Params` containing the following parameters:
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  removeFulfillmentGroupFromOrderUsingDELETE1(params: FulfillmentEndpointService.RemoveFulfillmentGroupFromOrderUsingDELETE1Params): __Observable<Array<FulfillmentGroupWrapper>> {
    return this.removeFulfillmentGroupFromOrderUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as Array<FulfillmentGroupWrapper>)
    );
  }

  /**
   * @param params The `FulfillmentEndpointService.UpdateFulfillmentGroupUsingPATCH1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  updateFulfillmentGroupUsingPATCH1Response(params: FulfillmentEndpointService.UpdateFulfillmentGroupUsingPATCH1Params): __Observable<__StrictHttpResponse<FulfillmentGroupWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;


    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'PATCH',
      this.rootUrl + `/shipping/${params.cartId}/group/${params.fulfillmentGroupId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FulfillmentGroupWrapper>;
      })
    );
  }
  /**
   * @param params The `FulfillmentEndpointService.UpdateFulfillmentGroupUsingPATCH1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  updateFulfillmentGroupUsingPATCH1(params: FulfillmentEndpointService.UpdateFulfillmentGroupUsingPATCH1Params): __Observable<FulfillmentGroupWrapper> {
    return this.updateFulfillmentGroupUsingPATCH1Response(params).pipe(
      __map(_r => _r.body as FulfillmentGroupWrapper)
    );
  }

  /**
   * @param params The `FulfillmentEndpointService.AddItemToFulfillmentGroupUsingPOST1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  addItemToFulfillmentGroupUsingPOST1Response(params: FulfillmentEndpointService.AddItemToFulfillmentGroupUsingPOST1Params): __Observable<__StrictHttpResponse<FulfillmentGroupWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;


    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/shipping/${params.cartId}/group/${params.fulfillmentGroupId}/item`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FulfillmentGroupWrapper>;
      })
    );
  }
  /**
   * @param params The `FulfillmentEndpointService.AddItemToFulfillmentGroupUsingPOST1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  addItemToFulfillmentGroupUsingPOST1(params: FulfillmentEndpointService.AddItemToFulfillmentGroupUsingPOST1Params): __Observable<FulfillmentGroupWrapper> {
    return this.addItemToFulfillmentGroupUsingPOST1Response(params).pipe(
      __map(_r => _r.body as FulfillmentGroupWrapper)
    );
  }

  /**
   * @param params The `FulfillmentEndpointService.RemoveOrderItemFromFulfillmentGroupUsingDELETE1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  removeOrderItemFromFulfillmentGroupUsingDELETE1Response(params: FulfillmentEndpointService.RemoveOrderItemFromFulfillmentGroupUsingDELETE1Params): __Observable<__StrictHttpResponse<FulfillmentGroupWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/shipping/${params.cartId}/group/${params.fulfillmentGroupId}/item/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FulfillmentGroupWrapper>;
      })
    );
  }
  /**
   * @param params The `FulfillmentEndpointService.RemoveOrderItemFromFulfillmentGroupUsingDELETE1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  removeOrderItemFromFulfillmentGroupUsingDELETE1(params: FulfillmentEndpointService.RemoveOrderItemFromFulfillmentGroupUsingDELETE1Params): __Observable<FulfillmentGroupWrapper> {
    return this.removeOrderItemFromFulfillmentGroupUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as FulfillmentGroupWrapper)
    );
  }

  /**
   * @param params The `FulfillmentEndpointService.AddFulfillmentOptionToFulfillmentGroupUsingPUT1Params` containing the following parameters:
   *
   * - `fulfillmentOptionId`: fulfillmentOptionId
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  addFulfillmentOptionToFulfillmentGroupUsingPUT1Response(params: FulfillmentEndpointService.AddFulfillmentOptionToFulfillmentGroupUsingPUT1Params): __Observable<__StrictHttpResponse<FulfillmentGroupWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/shipping/${params.cartId}/group/${params.fulfillmentGroupId}/option/${params.fulfillmentOptionId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FulfillmentGroupWrapper>;
      })
    );
  }
  /**
   * @param params The `FulfillmentEndpointService.AddFulfillmentOptionToFulfillmentGroupUsingPUT1Params` containing the following parameters:
   *
   * - `fulfillmentOptionId`: fulfillmentOptionId
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  addFulfillmentOptionToFulfillmentGroupUsingPUT1(params: FulfillmentEndpointService.AddFulfillmentOptionToFulfillmentGroupUsingPUT1Params): __Observable<FulfillmentGroupWrapper> {
    return this.addFulfillmentOptionToFulfillmentGroupUsingPUT1Response(params).pipe(
      __map(_r => _r.body as FulfillmentGroupWrapper)
    );
  }

  /**
   * @param cartId cartId
   * @return OK
   */
  findFulfillmentGroupsForOrderUsingGET1Response(cartId: number): __Observable<__StrictHttpResponse<Array<FulfillmentGroupWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/shipping/${cartId}/groups`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<FulfillmentGroupWrapper>>;
      })
    );
  }
  /**
   * @param cartId cartId
   * @return OK
   */
  findFulfillmentGroupsForOrderUsingGET1(cartId: number): __Observable<Array<FulfillmentGroupWrapper>> {
    return this.findFulfillmentGroupsForOrderUsingGET1Response(cartId).pipe(
      __map(_r => _r.body as Array<FulfillmentGroupWrapper>)
    );
  }

  /**
   * @param params The `FulfillmentEndpointService.RemoveAllFulfillmentGroupsFromOrderUsingDELETE1Params` containing the following parameters:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  removeAllFulfillmentGroupsFromOrderUsingDELETE1Response(params: FulfillmentEndpointService.RemoveAllFulfillmentGroupsFromOrderUsingDELETE1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.priceOrder != null) __params = __params.set('priceOrder', params.priceOrder.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/shipping/${params.cartId}/groups`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `FulfillmentEndpointService.RemoveAllFulfillmentGroupsFromOrderUsingDELETE1Params` containing the following parameters:
   *
   * - `cartId`: cartId
   *
   * - `priceOrder`: priceOrder
   *
   * @return OK
   */
  removeAllFulfillmentGroupsFromOrderUsingDELETE1(params: FulfillmentEndpointService.RemoveAllFulfillmentGroupsFromOrderUsingDELETE1Params): __Observable<OrderWrapper> {
    return this.removeAllFulfillmentGroupsFromOrderUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `FulfillmentEndpointService.UpdateFulfillmentGroupAddressUsingPUT1Params` containing the following parameters:
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * - `address`: address
   *
   * @return OK
   */
  updateFulfillmentGroupAddressUsingPUT1Response(params: FulfillmentEndpointService.UpdateFulfillmentGroupAddressUsingPUT1Params): __Observable<__StrictHttpResponse<FulfillmentGroupWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    __body = params.address;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/shipping/${params.cartId}/${params.fulfillmentGroupId}/address`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FulfillmentGroupWrapper>;
      })
    );
  }
  /**
   * @param params The `FulfillmentEndpointService.UpdateFulfillmentGroupAddressUsingPUT1Params` containing the following parameters:
   *
   * - `fulfillmentGroupId`: fulfillmentGroupId
   *
   * - `cartId`: cartId
   *
   * - `address`: address
   *
   * @return OK
   */
  updateFulfillmentGroupAddressUsingPUT1(params: FulfillmentEndpointService.UpdateFulfillmentGroupAddressUsingPUT1Params): __Observable<FulfillmentGroupWrapper> {
    return this.updateFulfillmentGroupAddressUsingPUT1Response(params).pipe(
      __map(_r => _r.body as FulfillmentGroupWrapper)
    );
  }
}

module FulfillmentEndpointService {

  /**
   * Parameters for addFulfillmentGroupToOrderUsingPOST1
   */
  export interface AddFulfillmentGroupToOrderUsingPOST1Params {

    /**
     * wrapper
     */
    wrapper: FulfillmentGroupWrapper;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for removeFulfillmentGroupFromOrderUsingDELETE1
   */
  export interface RemoveFulfillmentGroupFromOrderUsingDELETE1Params {

    /**
     * fulfillmentGroupId
     */
    fulfillmentGroupId: number;

    /**
     * cartId
     */
    cartId: number;
  }

  /**
   * Parameters for updateFulfillmentGroupUsingPATCH1
   */
  export interface UpdateFulfillmentGroupUsingPATCH1Params {

    /**
     * wrapper
     */
    wrapper: FulfillmentGroupWrapper;

    /**
     * fulfillmentGroupId
     */
    fulfillmentGroupId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for addItemToFulfillmentGroupUsingPOST1
   */
  export interface AddItemToFulfillmentGroupUsingPOST1Params {

    /**
     * wrapper
     */
    wrapper: FulfillmentGroupItemWrapper;

    /**
     * fulfillmentGroupId
     */
    fulfillmentGroupId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for removeOrderItemFromFulfillmentGroupUsingDELETE1
   */
  export interface RemoveOrderItemFromFulfillmentGroupUsingDELETE1Params {

    /**
     * itemId
     */
    itemId: number;

    /**
     * fulfillmentGroupId
     */
    fulfillmentGroupId: number;

    /**
     * cartId
     */
    cartId: number;
  }

  /**
   * Parameters for addFulfillmentOptionToFulfillmentGroupUsingPUT1
   */
  export interface AddFulfillmentOptionToFulfillmentGroupUsingPUT1Params {

    /**
     * fulfillmentOptionId
     */
    fulfillmentOptionId: number;

    /**
     * fulfillmentGroupId
     */
    fulfillmentGroupId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for removeAllFulfillmentGroupsFromOrderUsingDELETE1
   */
  export interface RemoveAllFulfillmentGroupsFromOrderUsingDELETE1Params {

    /**
     * cartId
     */
    cartId: number;

    /**
     * priceOrder
     */
    priceOrder?: boolean;
  }

  /**
   * Parameters for updateFulfillmentGroupAddressUsingPUT1
   */
  export interface UpdateFulfillmentGroupAddressUsingPUT1Params {

    /**
     * fulfillmentGroupId
     */
    fulfillmentGroupId: number;

    /**
     * cartId
     */
    cartId: number;

    /**
     * address
     */
    address: AddressWrapper;
  }
}

export { FulfillmentEndpointService }
