/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { RatingSummaryWrapper } from '../models/rating-summary-wrapper';
import { ReviewDetailWrapper } from '../models/review-detail-wrapper';

/**
 * Rating Endpoint
 */
@Injectable({
  providedIn: 'root',
})
class RatingEndpointService extends __BaseService {
  static readonly getRatingsForItemUsingGET1Path = '/ratings/{itemId}';
  static readonly getRatingsForItemUsingHEAD1Path = '/ratings/{itemId}';
  static readonly submitReviewForItemUsingPOST1Path = '/ratings/{itemId}';
  static readonly getRatingsForItemUsingPUT1Path = '/ratings/{itemId}';
  static readonly getRatingsForItemUsingDELETE1Path = '/ratings/{itemId}';
  static readonly getRatingsForItemUsingOPTIONS1Path = '/ratings/{itemId}';
  static readonly getRatingsForItemUsingPATCH1Path = '/ratings/{itemId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingGET1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingGET1Response(params: RatingEndpointService.GetRatingsForItemUsingGET1Params): __Observable<__StrictHttpResponse<RatingSummaryWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.ratingType != null) __params = __params.set('ratingType', params.ratingType.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/ratings/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RatingSummaryWrapper>;
      })
    );
  }
  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingGET1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingGET1(params: RatingEndpointService.GetRatingsForItemUsingGET1Params): __Observable<RatingSummaryWrapper> {
    return this.getRatingsForItemUsingGET1Response(params).pipe(
      __map(_r => _r.body as RatingSummaryWrapper)
    );
  }

  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingHEAD1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingHEAD1Response(params: RatingEndpointService.GetRatingsForItemUsingHEAD1Params): __Observable<__StrictHttpResponse<RatingSummaryWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.ratingType != null) __params = __params.set('ratingType', params.ratingType.toString());
    let req = new HttpRequest<any>(
      'HEAD',
      this.rootUrl + `/ratings/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RatingSummaryWrapper>;
      })
    );
  }
  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingHEAD1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingHEAD1(params: RatingEndpointService.GetRatingsForItemUsingHEAD1Params): __Observable<RatingSummaryWrapper> {
    return this.getRatingsForItemUsingHEAD1Response(params).pipe(
      __map(_r => _r.body as RatingSummaryWrapper)
    );
  }

  /**
   * @param params The `RatingEndpointService.SubmitReviewForItemUsingPOST1Params` containing the following parameters:
   *
   * - `reviewDetailWrapper`: reviewDetailWrapper
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  submitReviewForItemUsingPOST1Response(params: RatingEndpointService.SubmitReviewForItemUsingPOST1Params): __Observable<__StrictHttpResponse<RatingSummaryWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.reviewDetailWrapper;

    if (params.ratingType != null) __params = __params.set('ratingType', params.ratingType.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/ratings/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RatingSummaryWrapper>;
      })
    );
  }
  /**
   * @param params The `RatingEndpointService.SubmitReviewForItemUsingPOST1Params` containing the following parameters:
   *
   * - `reviewDetailWrapper`: reviewDetailWrapper
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  submitReviewForItemUsingPOST1(params: RatingEndpointService.SubmitReviewForItemUsingPOST1Params): __Observable<RatingSummaryWrapper> {
    return this.submitReviewForItemUsingPOST1Response(params).pipe(
      __map(_r => _r.body as RatingSummaryWrapper)
    );
  }

  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingPUT1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingPUT1Response(params: RatingEndpointService.GetRatingsForItemUsingPUT1Params): __Observable<__StrictHttpResponse<RatingSummaryWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.ratingType != null) __params = __params.set('ratingType', params.ratingType.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/ratings/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RatingSummaryWrapper>;
      })
    );
  }
  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingPUT1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingPUT1(params: RatingEndpointService.GetRatingsForItemUsingPUT1Params): __Observable<RatingSummaryWrapper> {
    return this.getRatingsForItemUsingPUT1Response(params).pipe(
      __map(_r => _r.body as RatingSummaryWrapper)
    );
  }

  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingDELETE1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingDELETE1Response(params: RatingEndpointService.GetRatingsForItemUsingDELETE1Params): __Observable<__StrictHttpResponse<RatingSummaryWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.ratingType != null) __params = __params.set('ratingType', params.ratingType.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/ratings/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RatingSummaryWrapper>;
      })
    );
  }
  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingDELETE1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingDELETE1(params: RatingEndpointService.GetRatingsForItemUsingDELETE1Params): __Observable<RatingSummaryWrapper> {
    return this.getRatingsForItemUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as RatingSummaryWrapper)
    );
  }

  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingOPTIONS1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingOPTIONS1Response(params: RatingEndpointService.GetRatingsForItemUsingOPTIONS1Params): __Observable<__StrictHttpResponse<RatingSummaryWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.ratingType != null) __params = __params.set('ratingType', params.ratingType.toString());
    let req = new HttpRequest<any>(
      'OPTIONS',
      this.rootUrl + `/ratings/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RatingSummaryWrapper>;
      })
    );
  }
  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingOPTIONS1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingOPTIONS1(params: RatingEndpointService.GetRatingsForItemUsingOPTIONS1Params): __Observable<RatingSummaryWrapper> {
    return this.getRatingsForItemUsingOPTIONS1Response(params).pipe(
      __map(_r => _r.body as RatingSummaryWrapper)
    );
  }

  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingPATCH1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingPATCH1Response(params: RatingEndpointService.GetRatingsForItemUsingPATCH1Params): __Observable<__StrictHttpResponse<RatingSummaryWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.ratingType != null) __params = __params.set('ratingType', params.ratingType.toString());
    let req = new HttpRequest<any>(
      'PATCH',
      this.rootUrl + `/ratings/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RatingSummaryWrapper>;
      })
    );
  }
  /**
   * @param params The `RatingEndpointService.GetRatingsForItemUsingPATCH1Params` containing the following parameters:
   *
   * - `itemId`: itemId
   *
   * - `ratingType`: ratingType
   *
   * @return OK
   */
  getRatingsForItemUsingPATCH1(params: RatingEndpointService.GetRatingsForItemUsingPATCH1Params): __Observable<RatingSummaryWrapper> {
    return this.getRatingsForItemUsingPATCH1Response(params).pipe(
      __map(_r => _r.body as RatingSummaryWrapper)
    );
  }
}

module RatingEndpointService {

  /**
   * Parameters for getRatingsForItemUsingGET1
   */
  export interface GetRatingsForItemUsingGET1Params {

    /**
     * itemId
     */
    itemId: string;

    /**
     * ratingType
     */
    ratingType?: string;
  }

  /**
   * Parameters for getRatingsForItemUsingHEAD1
   */
  export interface GetRatingsForItemUsingHEAD1Params {

    /**
     * itemId
     */
    itemId: string;

    /**
     * ratingType
     */
    ratingType?: string;
  }

  /**
   * Parameters for submitReviewForItemUsingPOST1
   */
  export interface SubmitReviewForItemUsingPOST1Params {

    /**
     * reviewDetailWrapper
     */
    reviewDetailWrapper: ReviewDetailWrapper;

    /**
     * itemId
     */
    itemId: string;

    /**
     * ratingType
     */
    ratingType?: string;
  }

  /**
   * Parameters for getRatingsForItemUsingPUT1
   */
  export interface GetRatingsForItemUsingPUT1Params {

    /**
     * itemId
     */
    itemId: string;

    /**
     * ratingType
     */
    ratingType?: string;
  }

  /**
   * Parameters for getRatingsForItemUsingDELETE1
   */
  export interface GetRatingsForItemUsingDELETE1Params {

    /**
     * itemId
     */
    itemId: string;

    /**
     * ratingType
     */
    ratingType?: string;
  }

  /**
   * Parameters for getRatingsForItemUsingOPTIONS1
   */
  export interface GetRatingsForItemUsingOPTIONS1Params {

    /**
     * itemId
     */
    itemId: string;

    /**
     * ratingType
     */
    ratingType?: string;
  }

  /**
   * Parameters for getRatingsForItemUsingPATCH1
   */
  export interface GetRatingsForItemUsingPATCH1Params {

    /**
     * itemId
     */
    itemId: string;

    /**
     * ratingType
     */
    ratingType?: string;
  }
}

export { RatingEndpointService }
