/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { OrderWrapper } from '../models/order-wrapper';
import { OrderPaymentWrapper } from '../models/order-payment-wrapper';
import { PaymentTransactionWrapper } from '../models/payment-transaction-wrapper';

/**
 * Checkout Endpoint
 */
@Injectable({
  providedIn: 'root',
})
class CheckoutEndpointService extends __BaseService {
  static readonly performCheckoutUsingPOST1Path = '/cart/checkout';
  static readonly addPaymentToOrderUsingPOST1Path = '/cart/checkout/payment';
  static readonly addPaymentToOrderByIdUsingPOST1Path = '/cart/checkout/payment/{customerPaymentId}';
  static readonly removePaymentFromOrderByIdUsingDELETE1Path = '/cart/checkout/payment/{paymentId}';
  static readonly addOrderPaymentTransactionUsingPUT1Path = '/cart/checkout/payment/{paymentId}/transaction';
  static readonly findPaymentsForOrderUsingGET1Path = '/cart/checkout/payments';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param cartId cartId
   * @return OK
   */
  performCheckoutUsingPOST1Response(cartId: number): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (cartId != null) __params = __params.set('cartId', cartId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cart/checkout`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param cartId cartId
   * @return OK
   */
  performCheckoutUsingPOST1(cartId: number): __Observable<OrderWrapper> {
    return this.performCheckoutUsingPOST1Response(cartId).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CheckoutEndpointService.AddPaymentToOrderUsingPOST1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  addPaymentToOrderUsingPOST1Response(params: CheckoutEndpointService.AddPaymentToOrderUsingPOST1Params): __Observable<__StrictHttpResponse<OrderPaymentWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;
    if (params.cartId != null) __params = __params.set('cartId', params.cartId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cart/checkout/payment`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderPaymentWrapper>;
      })
    );
  }
  /**
   * @param params The `CheckoutEndpointService.AddPaymentToOrderUsingPOST1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  addPaymentToOrderUsingPOST1(params: CheckoutEndpointService.AddPaymentToOrderUsingPOST1Params): __Observable<OrderPaymentWrapper> {
    return this.addPaymentToOrderUsingPOST1Response(params).pipe(
      __map(_r => _r.body as OrderPaymentWrapper)
    );
  }

  /**
   * @param params The `CheckoutEndpointService.AddPaymentToOrderByIdUsingPOST1Params` containing the following parameters:
   *
   * - `customerPaymentId`: customerPaymentId
   *
   * - `currency`: currency
   *
   * - `cartId`: cartId
   *
   * - `amount`: amount
   *
   * @return OK
   */
  addPaymentToOrderByIdUsingPOST1Response(params: CheckoutEndpointService.AddPaymentToOrderByIdUsingPOST1Params): __Observable<__StrictHttpResponse<OrderPaymentWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.currency != null) __params = __params.set('currency', params.currency.toString());
    if (params.cartId != null) __params = __params.set('cartId', params.cartId.toString());
    if (params.amount != null) __params = __params.set('amount', params.amount.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cart/checkout/payment/${params.customerPaymentId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderPaymentWrapper>;
      })
    );
  }
  /**
   * @param params The `CheckoutEndpointService.AddPaymentToOrderByIdUsingPOST1Params` containing the following parameters:
   *
   * - `customerPaymentId`: customerPaymentId
   *
   * - `currency`: currency
   *
   * - `cartId`: cartId
   *
   * - `amount`: amount
   *
   * @return OK
   */
  addPaymentToOrderByIdUsingPOST1(params: CheckoutEndpointService.AddPaymentToOrderByIdUsingPOST1Params): __Observable<OrderPaymentWrapper> {
    return this.addPaymentToOrderByIdUsingPOST1Response(params).pipe(
      __map(_r => _r.body as OrderPaymentWrapper)
    );
  }

  /**
   * @param params The `CheckoutEndpointService.RemovePaymentFromOrderByIdUsingDELETE1Params` containing the following parameters:
   *
   * - `paymentId`: paymentId
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  removePaymentFromOrderByIdUsingDELETE1Response(params: CheckoutEndpointService.RemovePaymentFromOrderByIdUsingDELETE1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.cartId != null) __params = __params.set('cartId', params.cartId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cart/checkout/payment/${params.paymentId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `CheckoutEndpointService.RemovePaymentFromOrderByIdUsingDELETE1Params` containing the following parameters:
   *
   * - `paymentId`: paymentId
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  removePaymentFromOrderByIdUsingDELETE1(params: CheckoutEndpointService.RemovePaymentFromOrderByIdUsingDELETE1Params): __Observable<OrderWrapper> {
    return this.removePaymentFromOrderByIdUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `CheckoutEndpointService.AddOrderPaymentTransactionUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `paymentId`: paymentId
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  addOrderPaymentTransactionUsingPUT1Response(params: CheckoutEndpointService.AddOrderPaymentTransactionUsingPUT1Params): __Observable<__StrictHttpResponse<OrderPaymentWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.wrapper;

    if (params.cartId != null) __params = __params.set('cartId', params.cartId.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cart/checkout/payment/${params.paymentId}/transaction`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderPaymentWrapper>;
      })
    );
  }
  /**
   * @param params The `CheckoutEndpointService.AddOrderPaymentTransactionUsingPUT1Params` containing the following parameters:
   *
   * - `wrapper`: wrapper
   *
   * - `paymentId`: paymentId
   *
   * - `cartId`: cartId
   *
   * @return OK
   */
  addOrderPaymentTransactionUsingPUT1(params: CheckoutEndpointService.AddOrderPaymentTransactionUsingPUT1Params): __Observable<OrderPaymentWrapper> {
    return this.addOrderPaymentTransactionUsingPUT1Response(params).pipe(
      __map(_r => _r.body as OrderPaymentWrapper)
    );
  }

  /**
   * @param cartId cartId
   * @return OK
   */
  findPaymentsForOrderUsingGET1Response(cartId: number): __Observable<__StrictHttpResponse<Array<OrderPaymentWrapper>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (cartId != null) __params = __params.set('cartId', cartId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cart/checkout/payments`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<OrderPaymentWrapper>>;
      })
    );
  }
  /**
   * @param cartId cartId
   * @return OK
   */
  findPaymentsForOrderUsingGET1(cartId: number): __Observable<Array<OrderPaymentWrapper>> {
    return this.findPaymentsForOrderUsingGET1Response(cartId).pipe(
      __map(_r => _r.body as Array<OrderPaymentWrapper>)
    );
  }
}

module CheckoutEndpointService {

  /**
   * Parameters for addPaymentToOrderUsingPOST1
   */
  export interface AddPaymentToOrderUsingPOST1Params {

    /**
     * wrapper
     */
    wrapper: OrderPaymentWrapper;

    /**
     * cartId
     */
    cartId: number;
  }

  /**
   * Parameters for addPaymentToOrderByIdUsingPOST1
   */
  export interface AddPaymentToOrderByIdUsingPOST1Params {

    /**
     * customerPaymentId
     */
    customerPaymentId: number;

    /**
     * currency
     */
    currency: string;

    /**
     * cartId
     */
    cartId: number;

    /**
     * amount
     */
    amount: number;
  }

  /**
   * Parameters for removePaymentFromOrderByIdUsingDELETE1
   */
  export interface RemovePaymentFromOrderByIdUsingDELETE1Params {

    /**
     * paymentId
     */
    paymentId: number;

    /**
     * cartId
     */
    cartId: number;
  }

  /**
   * Parameters for addOrderPaymentTransactionUsingPUT1
   */
  export interface AddOrderPaymentTransactionUsingPUT1Params {

    /**
     * wrapper
     */
    wrapper: PaymentTransactionWrapper;

    /**
     * paymentId
     */
    paymentId: number;

    /**
     * cartId
     */
    cartId: number;
  }
}

export { CheckoutEndpointService }
