/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { OrderWrapper } from '../models/order-wrapper';
import { ConfigurableOrderItemWrapper } from '../models/configurable-order-item-wrapper';
import { OrderItemWrapper } from '../models/order-item-wrapper';

/**
 * Wishlist Endpoint
 */
@Injectable({
  providedIn: 'root',
})
class WishlistEndpointService extends __BaseService {
  static readonly getWishlistUsingGET1Path = '/wishlist';
  static readonly moveListToCartUsingPOST1Path = '/wishlist';
  static readonly addConfigureItemToWishlistUsingPOST1Path = '/wishlist/configure-item';
  static readonly addItemToWishlistUsingPOST1Path = '/wishlist/item';
  static readonly updateQuantityInWishlistUsingPUT1Path = '/wishlist/items/{itemId}';
  static readonly removeItemFromWishlistUsingDELETE1Path = '/wishlist/items/{itemId}';
  static readonly moveItemToCartUsingPOST1Path = '/wishlist/items/{itemId}/move';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param wishlistName wishlistName
   * @return OK
   */
  getWishlistUsingGET1Response(wishlistName: string): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (wishlistName != null) __params = __params.set('wishlistName', wishlistName.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/wishlist`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param wishlistName wishlistName
   * @return OK
   */
  getWishlistUsingGET1(wishlistName: string): __Observable<OrderWrapper> {
    return this.getWishlistUsingGET1Response(wishlistName).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param wishlistName wishlistName
   * @return OK
   */
  moveListToCartUsingPOST1Response(wishlistName: string): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (wishlistName != null) __params = __params.set('wishlistName', wishlistName.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/wishlist`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param wishlistName wishlistName
   * @return OK
   */
  moveListToCartUsingPOST1(wishlistName: string): __Observable<OrderWrapper> {
    return this.moveListToCartUsingPOST1Response(wishlistName).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `WishlistEndpointService.AddConfigureItemToWishlistUsingPOST1Params` containing the following parameters:
   *
   * - `wishlistName`: wishlistName
   *
   * - `orderItemWrapper`: orderItemWrapper
   *
   * @return OK
   */
  addConfigureItemToWishlistUsingPOST1Response(params: WishlistEndpointService.AddConfigureItemToWishlistUsingPOST1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.wishlistName != null) __params = __params.set('wishlistName', params.wishlistName.toString());
    __body = params.orderItemWrapper;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/wishlist/configure-item`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `WishlistEndpointService.AddConfigureItemToWishlistUsingPOST1Params` containing the following parameters:
   *
   * - `wishlistName`: wishlistName
   *
   * - `orderItemWrapper`: orderItemWrapper
   *
   * @return OK
   */
  addConfigureItemToWishlistUsingPOST1(params: WishlistEndpointService.AddConfigureItemToWishlistUsingPOST1Params): __Observable<OrderWrapper> {
    return this.addConfigureItemToWishlistUsingPOST1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `WishlistEndpointService.AddItemToWishlistUsingPOST1Params` containing the following parameters:
   *
   * - `wishlistName`: wishlistName
   *
   * - `orderItemWrapper`: orderItemWrapper
   *
   * @return OK
   */
  addItemToWishlistUsingPOST1Response(params: WishlistEndpointService.AddItemToWishlistUsingPOST1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.wishlistName != null) __params = __params.set('wishlistName', params.wishlistName.toString());
    __body = params.orderItemWrapper;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/wishlist/item`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `WishlistEndpointService.AddItemToWishlistUsingPOST1Params` containing the following parameters:
   *
   * - `wishlistName`: wishlistName
   *
   * - `orderItemWrapper`: orderItemWrapper
   *
   * @return OK
   */
  addItemToWishlistUsingPOST1(params: WishlistEndpointService.AddItemToWishlistUsingPOST1Params): __Observable<OrderWrapper> {
    return this.addItemToWishlistUsingPOST1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `WishlistEndpointService.UpdateQuantityInWishlistUsingPUT1Params` containing the following parameters:
   *
   * - `wishlistName`: wishlistName
   *
   * - `quantity`: quantity
   *
   * - `itemId`: itemId
   *
   * @return OK
   */
  updateQuantityInWishlistUsingPUT1Response(params: WishlistEndpointService.UpdateQuantityInWishlistUsingPUT1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.wishlistName != null) __params = __params.set('wishlistName', params.wishlistName.toString());
    if (params.quantity != null) __params = __params.set('quantity', params.quantity.toString());

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/wishlist/items/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `WishlistEndpointService.UpdateQuantityInWishlistUsingPUT1Params` containing the following parameters:
   *
   * - `wishlistName`: wishlistName
   *
   * - `quantity`: quantity
   *
   * - `itemId`: itemId
   *
   * @return OK
   */
  updateQuantityInWishlistUsingPUT1(params: WishlistEndpointService.UpdateQuantityInWishlistUsingPUT1Params): __Observable<OrderWrapper> {
    return this.updateQuantityInWishlistUsingPUT1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `WishlistEndpointService.RemoveItemFromWishlistUsingDELETE1Params` containing the following parameters:
   *
   * - `wishlistName`: wishlistName
   *
   * - `itemId`: itemId
   *
   * @return OK
   */
  removeItemFromWishlistUsingDELETE1Response(params: WishlistEndpointService.RemoveItemFromWishlistUsingDELETE1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.wishlistName != null) __params = __params.set('wishlistName', params.wishlistName.toString());

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/wishlist/items/${params.itemId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `WishlistEndpointService.RemoveItemFromWishlistUsingDELETE1Params` containing the following parameters:
   *
   * - `wishlistName`: wishlistName
   *
   * - `itemId`: itemId
   *
   * @return OK
   */
  removeItemFromWishlistUsingDELETE1(params: WishlistEndpointService.RemoveItemFromWishlistUsingDELETE1Params): __Observable<OrderWrapper> {
    return this.removeItemFromWishlistUsingDELETE1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }

  /**
   * @param params The `WishlistEndpointService.MoveItemToCartUsingPOST1Params` containing the following parameters:
   *
   * - `wishlistName`: wishlistName
   *
   * - `itemId`: itemId
   *
   * @return OK
   */
  moveItemToCartUsingPOST1Response(params: WishlistEndpointService.MoveItemToCartUsingPOST1Params): __Observable<__StrictHttpResponse<OrderWrapper>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.wishlistName != null) __params = __params.set('wishlistName', params.wishlistName.toString());

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/wishlist/items/${params.itemId}/move`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<OrderWrapper>;
      })
    );
  }
  /**
   * @param params The `WishlistEndpointService.MoveItemToCartUsingPOST1Params` containing the following parameters:
   *
   * - `wishlistName`: wishlistName
   *
   * - `itemId`: itemId
   *
   * @return OK
   */
  moveItemToCartUsingPOST1(params: WishlistEndpointService.MoveItemToCartUsingPOST1Params): __Observable<OrderWrapper> {
    return this.moveItemToCartUsingPOST1Response(params).pipe(
      __map(_r => _r.body as OrderWrapper)
    );
  }
}

module WishlistEndpointService {

  /**
   * Parameters for addConfigureItemToWishlistUsingPOST1
   */
  export interface AddConfigureItemToWishlistUsingPOST1Params {

    /**
     * wishlistName
     */
    wishlistName: string;

    /**
     * orderItemWrapper
     */
    orderItemWrapper: ConfigurableOrderItemWrapper;
  }

  /**
   * Parameters for addItemToWishlistUsingPOST1
   */
  export interface AddItemToWishlistUsingPOST1Params {

    /**
     * wishlistName
     */
    wishlistName: string;

    /**
     * orderItemWrapper
     */
    orderItemWrapper: OrderItemWrapper;
  }

  /**
   * Parameters for updateQuantityInWishlistUsingPUT1
   */
  export interface UpdateQuantityInWishlistUsingPUT1Params {

    /**
     * wishlistName
     */
    wishlistName: string;

    /**
     * quantity
     */
    quantity: number;

    /**
     * itemId
     */
    itemId: number;
  }

  /**
   * Parameters for removeItemFromWishlistUsingDELETE1
   */
  export interface RemoveItemFromWishlistUsingDELETE1Params {

    /**
     * wishlistName
     */
    wishlistName: string;

    /**
     * itemId
     */
    itemId: number;
  }

  /**
   * Parameters for moveItemToCartUsingPOST1
   */
  export interface MoveItemToCartUsingPOST1Params {

    /**
     * wishlistName
     */
    wishlistName: string;

    /**
     * itemId
     */
    itemId: string;
  }
}

export { WishlistEndpointService }
