/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';


/**
 * Basic Error Controller
 */
@Injectable({
  providedIn: 'root',
})
class BasicErrorControllerService extends __BaseService {
  static readonly errorUsingGET1Path = '/error';
  static readonly errorUsingHEAD1Path = '/error';
  static readonly errorUsingPOST1Path = '/error';
  static readonly errorUsingPUT1Path = '/error';
  static readonly errorUsingDELETE1Path = '/error';
  static readonly errorUsingOPTIONS1Path = '/error';
  static readonly errorUsingPATCH1Path = '/error';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @return OK
   */
  errorUsingGET1Response(): __Observable<__StrictHttpResponse<{[key: string]: {}}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/error`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{[key: string]: {}}>;
      })
    );
  }
  /**
   * @return OK
   */
  errorUsingGET1(): __Observable<{[key: string]: {}}> {
    return this.errorUsingGET1Response().pipe(
      __map(_r => _r.body as {[key: string]: {}})
    );
  }

  /**
   * @return OK
   */
  errorUsingHEAD1Response(): __Observable<__StrictHttpResponse<{[key: string]: {}}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'HEAD',
      this.rootUrl + `/error`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{[key: string]: {}}>;
      })
    );
  }
  /**
   * @return OK
   */
  errorUsingHEAD1(): __Observable<{[key: string]: {}}> {
    return this.errorUsingHEAD1Response().pipe(
      __map(_r => _r.body as {[key: string]: {}})
    );
  }

  /**
   * @return OK
   */
  errorUsingPOST1Response(): __Observable<__StrictHttpResponse<{[key: string]: {}}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/error`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{[key: string]: {}}>;
      })
    );
  }
  /**
   * @return OK
   */
  errorUsingPOST1(): __Observable<{[key: string]: {}}> {
    return this.errorUsingPOST1Response().pipe(
      __map(_r => _r.body as {[key: string]: {}})
    );
  }

  /**
   * @return OK
   */
  errorUsingPUT1Response(): __Observable<__StrictHttpResponse<{[key: string]: {}}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/error`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{[key: string]: {}}>;
      })
    );
  }
  /**
   * @return OK
   */
  errorUsingPUT1(): __Observable<{[key: string]: {}}> {
    return this.errorUsingPUT1Response().pipe(
      __map(_r => _r.body as {[key: string]: {}})
    );
  }

  /**
   * @return OK
   */
  errorUsingDELETE1Response(): __Observable<__StrictHttpResponse<{[key: string]: {}}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/error`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{[key: string]: {}}>;
      })
    );
  }
  /**
   * @return OK
   */
  errorUsingDELETE1(): __Observable<{[key: string]: {}}> {
    return this.errorUsingDELETE1Response().pipe(
      __map(_r => _r.body as {[key: string]: {}})
    );
  }

  /**
   * @return OK
   */
  errorUsingOPTIONS1Response(): __Observable<__StrictHttpResponse<{[key: string]: {}}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'OPTIONS',
      this.rootUrl + `/error`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{[key: string]: {}}>;
      })
    );
  }
  /**
   * @return OK
   */
  errorUsingOPTIONS1(): __Observable<{[key: string]: {}}> {
    return this.errorUsingOPTIONS1Response().pipe(
      __map(_r => _r.body as {[key: string]: {}})
    );
  }

  /**
   * @return OK
   */
  errorUsingPATCH1Response(): __Observable<__StrictHttpResponse<{[key: string]: {}}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'PATCH',
      this.rootUrl + `/error`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{[key: string]: {}}>;
      })
    );
  }
  /**
   * @return OK
   */
  errorUsingPATCH1(): __Observable<{[key: string]: {}}> {
    return this.errorUsingPATCH1Response().pipe(
      __map(_r => _r.body as {[key: string]: {}})
    );
  }
}

module BasicErrorControllerService {
}

export { BasicErrorControllerService }
