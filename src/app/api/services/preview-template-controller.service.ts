/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';


/**
 * Preview Template Controller
 */
@Injectable({
  providedIn: 'root',
})
class PreviewTemplateControllerService extends __BaseService {
  static readonly displayPreviewUsingGET1Path = '/preview/**';
  static readonly displayPreviewUsingHEAD1Path = '/preview/**';
  static readonly displayPreviewUsingPOST1Path = '/preview/**';
  static readonly displayPreviewUsingPUT1Path = '/preview/**';
  static readonly displayPreviewUsingDELETE1Path = '/preview/**';
  static readonly displayPreviewUsingOPTIONS1Path = '/preview/**';
  static readonly displayPreviewUsingPATCH1Path = '/preview/**';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @return OK
   */
  displayPreviewUsingGET1Response(): __Observable<__StrictHttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/preview/**`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<string>;
      })
    );
  }
  /**
   * @return OK
   */
  displayPreviewUsingGET1(): __Observable<string> {
    return this.displayPreviewUsingGET1Response().pipe(
      __map(_r => _r.body as string)
    );
  }

  /**
   * @return OK
   */
  displayPreviewUsingHEAD1Response(): __Observable<__StrictHttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'HEAD',
      this.rootUrl + `/preview/**`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<string>;
      })
    );
  }
  /**
   * @return OK
   */
  displayPreviewUsingHEAD1(): __Observable<string> {
    return this.displayPreviewUsingHEAD1Response().pipe(
      __map(_r => _r.body as string)
    );
  }

  /**
   * @return OK
   */
  displayPreviewUsingPOST1Response(): __Observable<__StrictHttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/preview/**`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<string>;
      })
    );
  }
  /**
   * @return OK
   */
  displayPreviewUsingPOST1(): __Observable<string> {
    return this.displayPreviewUsingPOST1Response().pipe(
      __map(_r => _r.body as string)
    );
  }

  /**
   * @return OK
   */
  displayPreviewUsingPUT1Response(): __Observable<__StrictHttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/preview/**`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<string>;
      })
    );
  }
  /**
   * @return OK
   */
  displayPreviewUsingPUT1(): __Observable<string> {
    return this.displayPreviewUsingPUT1Response().pipe(
      __map(_r => _r.body as string)
    );
  }

  /**
   * @return OK
   */
  displayPreviewUsingDELETE1Response(): __Observable<__StrictHttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/preview/**`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<string>;
      })
    );
  }
  /**
   * @return OK
   */
  displayPreviewUsingDELETE1(): __Observable<string> {
    return this.displayPreviewUsingDELETE1Response().pipe(
      __map(_r => _r.body as string)
    );
  }

  /**
   * @return OK
   */
  displayPreviewUsingOPTIONS1Response(): __Observable<__StrictHttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'OPTIONS',
      this.rootUrl + `/preview/**`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<string>;
      })
    );
  }
  /**
   * @return OK
   */
  displayPreviewUsingOPTIONS1(): __Observable<string> {
    return this.displayPreviewUsingOPTIONS1Response().pipe(
      __map(_r => _r.body as string)
    );
  }

  /**
   * @return OK
   */
  displayPreviewUsingPATCH1Response(): __Observable<__StrictHttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'PATCH',
      this.rootUrl + `/preview/**`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<string>;
      })
    );
  }
  /**
   * @return OK
   */
  displayPreviewUsingPATCH1(): __Observable<string> {
    return this.displayPreviewUsingPATCH1Response().pipe(
      __map(_r => _r.body as string)
    );
  }
}

module PreviewTemplateControllerService {
}

export { PreviewTemplateControllerService }
