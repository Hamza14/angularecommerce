/* tslint:disable */
import { ApplicationContext } from './application-context';
export interface RatingDetailWrapper {
  applicationContext?: ApplicationContext;
  id?: number;
  rating?: number;
  ratingSubmittedDate?: string;
}
