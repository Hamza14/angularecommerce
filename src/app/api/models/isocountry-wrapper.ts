/* tslint:disable */
export interface ISOCountryWrapper {
  alpha2?: string;
  name?: string;
}
