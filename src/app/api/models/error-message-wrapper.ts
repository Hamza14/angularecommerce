/* tslint:disable */
export interface ErrorMessageWrapper {
  message?: string;
  messageKey?: string;
}
