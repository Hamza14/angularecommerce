/* tslint:disable */
import { ApplicationContext } from './application-context';
export interface OrderItemQualifierWrapper {
  applicationContext?: ApplicationContext;
  offerId?: number;
  quantity?: number;
}
