/* tslint:disable */
export interface OrderAttributeWrapper {
  id?: number;
  name?: string;
  orderId?: number;
  value?: string;
}
