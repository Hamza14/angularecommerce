/* tslint:disable */
import { ApplicationContext } from './application-context';
import { Character } from './character';
export interface ProductAttributeWrapper {
  applicationContext?: ApplicationContext;
  archived?: Character;
  attributeName?: string;
  attributeValue?: string;
  id?: number;
  productId?: number;
}
