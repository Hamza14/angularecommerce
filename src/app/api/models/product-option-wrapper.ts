/* tslint:disable */
import { ProductOptionValueWrapper } from './product-option-value-wrapper';
import { ApplicationContext } from './application-context';
import { Character } from './character';
export interface ProductOptionWrapper {
  allowedValues?: Array<ProductOptionValueWrapper>;
  applicationContext?: ApplicationContext;
  archived?: Character;
  attributeName?: string;
  label?: string;
  productOptionType?: string;
  productOptionValidationStrategyType?: string;
  productOptionValidationType?: string;
  required?: boolean;
  validationString?: string;
}
