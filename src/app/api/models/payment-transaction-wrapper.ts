/* tslint:disable */
import { MapElementWrapper } from './map-element-wrapper';
import { Character } from './character';
export interface PaymentTransactionWrapper {
  id?: number;
  additionalFields?: Array<MapElementWrapper>;
  archived?: Character;
  currency?: string;
  customerIpAddress?: string;
  amount?: number;
  orderPaymentId?: number;
  parentTransactionId?: number;
  rawResponse?: string;
  success?: boolean;
  type?: string;
}
