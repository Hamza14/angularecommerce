/* tslint:disable */
import { ApplicationContext } from './application-context';
import { Character } from './character';
import { Money } from './money';
export interface ProductOptionValueWrapper {
  applicationContext?: ApplicationContext;
  archived?: Character;
  attributeValue?: string;
  priceAdjustment?: Money;
  productOptionId?: number;
}
