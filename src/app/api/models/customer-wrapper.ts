/* tslint:disable */
import { CustomerAttributeWrapper } from './customer-attribute-wrapper';
export interface CustomerWrapper {
  customerAttributes?: Array<CustomerAttributeWrapper>;
  emailAddress?: string;
  firstName?: string;
  id?: number;
  lastName?: string;
  registered?: boolean;
}
