/* tslint:disable */
import { CountryWrapper } from './country-wrapper';
import { ISOCountryWrapper } from './isocountry-wrapper';
import { PhoneWrapper } from './phone-wrapper';
import { StateWrapper } from './state-wrapper';
export interface AddressWrapper {
  isDefault?: boolean;
  addressLine1?: string;
  addressLine3?: string;
  city?: string;
  companyName?: string;
  country?: CountryWrapper;
  firstName?: string;
  id?: number;
  isBusiness?: boolean;
  addressLine2?: string;
  isoCountryAlpha2?: ISOCountryWrapper;
  isoCountrySubdivision?: string;
  lastName?: string;
  phoneFax?: PhoneWrapper;
  phonePrimary?: PhoneWrapper;
  phoneSecondary?: PhoneWrapper;
  postalCode?: string;
  state?: StateWrapper;
  stateProvinceRegion?: string;
}
