/* tslint:disable */
export interface Environment {
  activeProfiles?: Array<string>;
  defaultProfiles?: Array<string>;
}
