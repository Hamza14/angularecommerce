/* tslint:disable */
import { MapElementWrapper } from './map-element-wrapper';
import { AddressWrapper } from './address-wrapper';
import { CustomerWrapper } from './customer-wrapper';
import { ApplicationContext } from './application-context';
export interface CustomerPaymentWrapper {
  expirationDate?: string;
  additionalFields?: Array<MapElementWrapper>;
  billingAddress?: AddressWrapper;
  cardName?: string;
  cardType?: string;
  customer?: CustomerWrapper;
  applicationContext?: ApplicationContext;
  id?: number;
  isDefault?: boolean;
  lastFour?: string;
  paymentGatewayType?: string;
  paymentToken?: string;
  paymentType?: string;
}
