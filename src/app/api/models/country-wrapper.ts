/* tslint:disable */
export interface CountryWrapper {
  abbreviation?: string;
  name?: string;
}
