/* tslint:disable */
import { ErrorMessageWrapper } from './error-message-wrapper';
export interface ErrorWrapper {
  httpStatusCode?: number;
  messages?: Array<ErrorMessageWrapper>;
}
