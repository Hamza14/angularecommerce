/* tslint:disable */
import { OrderItemAttributeWrapper } from './order-item-attribute-wrapper';
import { ApplicationContext } from './application-context';
import { Money } from './money';
import { OrderItemPriceDetailWrapper } from './order-item-price-detail-wrapper';
import { MediaWrapper } from './media-wrapper';
import { OrderItemQualifierWrapper } from './order-item-qualifier-wrapper';
export interface OrderItemWrapper {
  orderItemAttributes?: Array<OrderItemAttributeWrapper>;
  applicationContext?: ApplicationContext;
  bundle?: boolean;
  bundleItems?: Array<OrderItemWrapper>;
  cartMessages?: Array<string>;
  categoryId?: number;
  childOrderItems?: Array<OrderItemWrapper>;
  discountingAllowed?: boolean;
  hasValidationError?: boolean;
  id?: number;
  isBundle?: boolean;
  isDiscountingAllowed?: boolean;
  name?: string;
  orderId?: number;
  averagePrice?: Money;
  orderItemPriceDetails?: Array<OrderItemPriceDetailWrapper>;
  parentOrderItemId?: number;
  priceBeforeAdjustments?: Money;
  primaryMedia?: MediaWrapper;
  productId?: number;
  productUrl?: string;
  qualifiers?: Array<OrderItemQualifierWrapper>;
  quantity?: number;
  retailPrice?: Money;
  salePrice?: Money;
  skuId?: number;
  totalAdjustmentValue?: Money;
  totalPrice?: Money;
}
