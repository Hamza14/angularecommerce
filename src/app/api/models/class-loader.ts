/* tslint:disable */
export interface ClassLoader {
  defaultAssertionStatus?: boolean;
  parent?: ClassLoader;
}
