/* tslint:disable */
import { DimensionWrapper } from './dimension-wrapper';
import { ApplicationContext } from './application-context';
import { Character } from './character';
import { Money } from './money';
import { WeightWrapper } from './weight-wrapper';
export interface SkuWrapper {
  dimension?: DimensionWrapper;
  active?: boolean;
  activeStartDate?: string;
  applicationContext?: ApplicationContext;
  archived?: Character;
  available?: boolean;
  description?: string;
  activeEndDate?: string;
  id?: number;
  inventoryType?: string;
  name?: string;
  retailPrice?: Money;
  salePrice?: Money;
  weight?: WeightWrapper;
}
