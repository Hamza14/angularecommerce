/* tslint:disable */
import { ApplicationContext } from './application-context';
import { Money } from './money';
export interface DynamicSkuPricesWrapper {
  applicationContext?: ApplicationContext;
  priceAdjustment?: Money;
  retailPrice?: Money;
  salePrice?: Money;
}
