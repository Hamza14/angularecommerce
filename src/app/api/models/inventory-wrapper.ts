/* tslint:disable */
export interface InventoryWrapper {
  inventoryType?: string;
  quantityAvailable?: number;
  skuId?: number;
}
