/* tslint:disable */
import { CategoryWrapper } from './category-wrapper';
export interface CategoriesWrapper {
  categories?: Array<CategoryWrapper>;
}
