/* tslint:disable */
import { Character } from './character';
export interface SkuAttributeWrapper {
  archived?: Character;
  attributeName?: string;
  attributeValue?: string;
  id?: number;
  skuId?: number;
}
