/* tslint:disable */
export interface PhoneWrapper {
  id?: number;
  isActive?: boolean;
  isDefault?: boolean;
  phoneNumber?: string;
}
