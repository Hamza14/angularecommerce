/* tslint:disable */
import { ApplicationContext } from './application-context';
export interface OrderItemAttributeWrapper {
  applicationContext?: ApplicationContext;
  id?: number;
  name?: string;
  orderItemId?: number;
  value?: string;
}
