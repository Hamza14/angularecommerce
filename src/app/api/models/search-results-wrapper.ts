/* tslint:disable */
import { ProductWrapper } from './product-wrapper';
import { SearchFacetWrapper } from './search-facet-wrapper';
export interface SearchResultsWrapper {
  page?: number;
  pageSize?: number;
  products?: Array<ProductWrapper>;
  searchFacets?: Array<SearchFacetWrapper>;
  totalPages?: number;
  totalResults?: number;
}
