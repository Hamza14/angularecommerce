/* tslint:disable */
import { ApplicationContext } from './application-context';
import { AdjustmentWrapper } from './adjustment-wrapper';
import { Money } from './money';
export interface OrderItemPriceDetailWrapper {
  applicationContext?: ApplicationContext;
  id?: number;
  orderItemPriceDetailAdjustments?: Array<AdjustmentWrapper>;
  quantity?: number;
  totalAdjustedPrice?: Money;
  totalAdjustmentValue?: Money;
}
