/* tslint:disable */
import { ApplicationContext } from './application-context';
import { MediaWrapper } from './media-wrapper';
export interface PromotionMessageDTOWrapper {
  applicationContext?: ApplicationContext;
  localeCode?: string;
  media?: MediaWrapper;
  message?: string;
  messagePlacement?: string;
  priority?: number;
}
