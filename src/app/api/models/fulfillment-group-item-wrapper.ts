/* tslint:disable */
import { OrderItemWrapper } from './order-item-wrapper';
import { TaxDetailWrapper } from './tax-detail-wrapper';
import { Money } from './money';
export interface FulfillmentGroupItemWrapper {
  fulfillmentGroupId?: number;
  id?: number;
  orderItem?: OrderItemWrapper;
  orderItemId?: number;
  quantity?: number;
  taxDetails?: Array<TaxDetailWrapper>;
  totalItemAmount?: Money;
  totalTax?: Money;
}
