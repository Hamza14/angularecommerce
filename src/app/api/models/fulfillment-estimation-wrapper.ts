/* tslint:disable */
import { FulfillmentOptionWrapper } from './fulfillment-option-wrapper';
import { Money } from './money';
export interface FulfillmentEstimationWrapper {
  fulfillmentOption?: FulfillmentOptionWrapper;
  price?: Money;
}
