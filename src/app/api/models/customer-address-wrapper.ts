/* tslint:disable */
import { AddressWrapper } from './address-wrapper';
import { ApplicationContext } from './application-context';
import { Character } from './character';
export interface CustomerAddressWrapper {
  address?: AddressWrapper;
  addressName?: string;
  applicationContext?: ApplicationContext;
  archived?: Character;
  id?: number;
}
