/* tslint:disable */
import { Money } from './money';
import { BroadleafEnumerationTypeWrapper } from './broadleaf-enumeration-type-wrapper';
export interface TaxDetailWrapper {
  amount?: Money;
  country?: string;
  currency?: string;
  id?: number;
  jurisdictionName?: string;
  rate?: number;
  region?: string;
  taxName?: string;
  taxType?: BroadleafEnumerationTypeWrapper;
}
