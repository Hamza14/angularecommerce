/* tslint:disable */
import { ApplicationContext } from './application-context';
import { Character } from './character';
export interface MediaWrapper {
  allowOverrideUrl?: boolean;
  altText?: string;
  applicationContext?: ApplicationContext;
  archived?: Character;
  id?: number;
  tags?: string;
  title?: string;
  url?: string;
}
