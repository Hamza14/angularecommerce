/* tslint:disable */
export interface SearchFacetValueWrapper {
  active?: boolean;
  maxValue?: number;
  minValue?: number;
  quantity?: number;
  value?: string;
  valueKey?: string;
}
