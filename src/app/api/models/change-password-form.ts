/* tslint:disable */
export interface ChangePasswordForm {
  currentPassword?: string;
  newPassword?: string;
  newPasswordConfirm?: string;
}
