/* tslint:disable */
import { OrderAttributeWrapper } from './order-attribute-wrapper';
import { CartMessageWrapper } from './cart-message-wrapper';
import { Money } from './money';
import { FulfillmentGroupWrapper } from './fulfillment-group-wrapper';
import { AdjustmentWrapper } from './adjustment-wrapper';
import { CustomerWrapper } from './customer-wrapper';
import { OrderItemWrapper } from './order-item-wrapper';
import { OrderPaymentWrapper } from './order-payment-wrapper';
export interface OrderWrapper {
  orderAttributes?: Array<OrderAttributeWrapper>;
  cartMessages?: Array<CartMessageWrapper>;
  emailAddress?: string;
  fulfillmentGroupAdjustmentsValue?: Money;
  fulfillmentGroups?: Array<FulfillmentGroupWrapper>;
  id?: number;
  itemAdjustmentsValue?: Money;
  itemCount?: number;
  orderAdjustments?: Array<AdjustmentWrapper>;
  orderAdjustmentsValue?: Money;
  customer?: CustomerWrapper;
  orderItems?: Array<OrderItemWrapper>;
  orderNumber?: string;
  payments?: Array<OrderPaymentWrapper>;
  status?: string;
  subTotal?: Money;
  submitDate?: string;
  total?: Money;
  totalAdjustmentsValue?: Money;
  totalShipping?: Money;
  totalTax?: Money;
}
