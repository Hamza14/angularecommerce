/* tslint:disable */
import { ApplicationContext } from './application-context';
export interface DimensionWrapper {
  applicationContext?: ApplicationContext;
  container?: string;
  depth?: number;
  dimensionUnitOfMeasure?: string;
  girth?: number;
  height?: number;
  size?: string;
  width?: number;
}
