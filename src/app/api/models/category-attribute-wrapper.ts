/* tslint:disable */
export interface CategoryAttributeWrapper {
  attributeName?: string;
  attributeValue?: string;
  categoryId?: number;
  id?: number;
}
