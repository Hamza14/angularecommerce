/* tslint:disable */
import { ReviewDetailWrapper } from './review-detail-wrapper';
import { RatingDetailWrapper } from './rating-detail-wrapper';
export interface RatingSummaryWrapper {
  averageRating?: number;
  currentCustomerReview?: ReviewDetailWrapper;
  id?: number;
  itemId?: string;
  numberOfRatings?: number;
  numberOfReviews?: number;
  ratingType?: string;
  ratings?: Array<RatingDetailWrapper>;
  reviews?: Array<ReviewDetailWrapper>;
}
