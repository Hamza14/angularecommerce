/* tslint:disable */
import { ApplicationContext } from './application-context';
import { Character } from './character';
import { ProductWrapper } from './product-wrapper';
export interface RelatedProductWrapper {
  applicationContext?: ApplicationContext;
  archived?: Character;
  id?: number;
  product?: ProductWrapper;
  promotionalMessage?: string;
  sequence?: number;
}
