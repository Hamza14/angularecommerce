/* tslint:disable */
import { AddressWrapper } from './address-wrapper';
import { AdjustmentWrapper } from './adjustment-wrapper';
import { FulfillmentGroupItemWrapper } from './fulfillment-group-item-wrapper';
import { FulfillmentOptionWrapper } from './fulfillment-option-wrapper';
import { BroadleafEnumerationTypeWrapper } from './broadleaf-enumeration-type-wrapper';
import { PhoneWrapper } from './phone-wrapper';
import { TaxDetailWrapper } from './tax-detail-wrapper';
import { Money } from './money';
export interface FulfillmentGroupWrapper {
  address?: AddressWrapper;
  fulfillmentGroupAdjustments?: Array<AdjustmentWrapper>;
  fulfillmentGroupItems?: Array<FulfillmentGroupItemWrapper>;
  fulfillmentOption?: FulfillmentOptionWrapper;
  fulfillmentType?: BroadleafEnumerationTypeWrapper;
  id?: number;
  orderId?: number;
  phone?: PhoneWrapper;
  taxDetails?: Array<TaxDetailWrapper>;
  total?: Money;
}
