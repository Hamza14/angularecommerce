/* tslint:disable */
import { Character } from './character';
import { ApplicationContext } from './application-context';
import { Money } from './money';
import { SkuWrapper } from './sku-wrapper';
export interface SkuBundleItemWrapper {
  longDescription?: string;
  active?: boolean;
  archived?: Character;
  bundleId?: number;
  description?: string;
  id?: number;
  applicationContext?: ApplicationContext;
  name?: string;
  productId?: number;
  quantity?: number;
  retailPrice?: Money;
  salePrice?: Money;
  sku?: SkuWrapper;
}
