/* tslint:disable */
import { Character } from './character';
import { BroadleafEnumerationTypeWrapper } from './broadleaf-enumeration-type-wrapper';
export interface FulfillmentOptionWrapper {
  archived?: Character;
  description?: string;
  fulfillmentType?: BroadleafEnumerationTypeWrapper;
  id?: number;
  name?: string;
}
