/* tslint:disable */
import { Money } from './money';
import { ApplicationContext } from './application-context';
export interface AdjustmentWrapper {
  adjustmentValue?: Money;
  applicationContext?: ApplicationContext;
  discountAmount?: number;
  discountType?: string;
  id?: number;
  marketingMessage?: string;
  offerid?: number;
  reason?: string;
}
