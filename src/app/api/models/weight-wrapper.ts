/* tslint:disable */
export interface WeightWrapper {
  unitOfMeasure?: string;
  weight?: number;
}
