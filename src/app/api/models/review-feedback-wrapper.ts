/* tslint:disable */
import { ApplicationContext } from './application-context';
export interface ReviewFeedbackWrapper {
  applicationContext?: ApplicationContext;
  helpful?: boolean;
  id?: number;
}
