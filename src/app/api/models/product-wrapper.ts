/* tslint:disable */
import { SkuWrapper } from './sku-wrapper';
import { ApplicationContext } from './application-context';
import { Character } from './character';
import { Money } from './money';
import { RelatedProductWrapper } from './related-product-wrapper';
import { DynamicSkuPricesWrapper } from './dynamic-sku-prices-wrapper';
import { MediaWrapper } from './media-wrapper';
import { ProductAttributeWrapper } from './product-attribute-wrapper';
import { ProductOptionWrapper } from './product-option-wrapper';
import { PromotionMessageDTOWrapper } from './promotion-message-dtowrapper';
import { SkuBundleItemWrapper } from './sku-bundle-item-wrapper';
export interface ProductWrapper {
  manufacturer?: string;
  active?: boolean;
  activeStartDate?: string;
  additionalSkus?: Array<SkuWrapper>;
  applicationContext?: ApplicationContext;
  archived?: Character;
  bundleItemsRetailPrice?: Money;
  bundleItemsSalePrice?: Money;
  crossSaleProducts?: Array<RelatedProductWrapper>;
  defaultCategoryId?: number;
  defaultSku?: SkuWrapper;
  description?: string;
  dynamicSkuPrices?: DynamicSkuPricesWrapper;
  id?: number;
  longDescription?: string;
  activeEndDate?: string;
  media?: Array<MediaWrapper>;
  model?: string;
  name?: string;
  primaryMedia?: MediaWrapper;
  priority?: number;
  productAttributes?: Array<ProductAttributeWrapper>;
  productOptions?: Array<ProductOptionWrapper>;
  promoMessage?: string;
  promotionMessages?: {[key: string]: Array<PromotionMessageDTOWrapper>};
  retailPrice?: Money;
  salePrice?: Money;
  skuBundleItems?: Array<SkuBundleItemWrapper>;
  upsaleProducts?: Array<RelatedProductWrapper>;
  url?: string;
}
