/* tslint:disable */
export interface BroadleafEnumerationTypeWrapper {
  friendlyName?: string;
  type?: string;
}
