/* tslint:disable */
import { SearchFacetValueWrapper } from './search-facet-value-wrapper';
export interface SearchFacetWrapper {
  active?: boolean;
  fieldName?: string;
  label?: string;
  values?: Array<SearchFacetValueWrapper>;
}
