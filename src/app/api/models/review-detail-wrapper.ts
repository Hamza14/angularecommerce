/* tslint:disable */
import { ApplicationContext } from './application-context';
import { RatingDetailWrapper } from './rating-detail-wrapper';
import { ReviewFeedbackWrapper } from './review-feedback-wrapper';
export interface ReviewDetailWrapper {
  applicationContext?: ApplicationContext;
  author?: string;
  helpfulCount?: number;
  id?: number;
  notHelpfulCount?: number;
  ratingDetail?: RatingDetailWrapper;
  reviewFeedback?: Array<ReviewFeedbackWrapper>;
  reviewStatus?: string;
  reviewSubmittedDate?: string;
  reviewText?: string;
}
