/* tslint:disable */
import { MapElementWrapper } from './map-element-wrapper';
import { Money } from './money';
import { OrderItemAttributeWrapper } from './order-item-attribute-wrapper';
import { ApplicationContext } from './application-context';
import { ProductWrapper } from './product-wrapper';
export interface ConfigurableOrderItemWrapper {
  orderItemIndex?: number;
  additionalAttributes?: Array<MapElementWrapper>;
  childOrderItems?: Array<ConfigurableOrderItemWrapper>;
  displayPrice?: Money;
  hasConfigurationError?: boolean;
  itemAttributes?: Array<OrderItemAttributeWrapper>;
  maxQuantity?: number;
  minQuantity?: number;
  multiSelect?: boolean;
  orderItemId?: number;
  applicationContext?: ApplicationContext;
  originalOrderItemId?: number;
  overrideRetailPrice?: Money;
  overrideSalePrice?: Money;
  parentOrderItemId?: number;
  product?: ProductWrapper;
  productChoices?: Array<ConfigurableOrderItemWrapper>;
  productId?: number;
  quantity?: number;
  skuId?: number;
  updateRequest?: boolean;
}
