/* tslint:disable */
import { ClassLoader } from './class-loader';
import { AutowireCapableBeanFactory } from './autowire-capable-bean-factory';
import { Environment } from './environment';
import { BeanFactory } from './bean-factory';
export interface ApplicationContext {
  displayName?: string;
  applicationName?: string;
  beanDefinitionCount?: number;
  beanDefinitionNames?: Array<string>;
  classLoader?: ClassLoader;
  autowireCapableBeanFactory?: AutowireCapableBeanFactory;
  environment?: Environment;
  id?: string;
  parent?: ApplicationContext;
  parentBeanFactory?: BeanFactory;
  startupDate?: number;
}
