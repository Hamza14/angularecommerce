/* tslint:disable */
export interface CustomerAttributeWrapper {
  customerId?: number;
  id?: number;
  name?: string;
  value?: string;
}
