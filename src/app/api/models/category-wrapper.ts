/* tslint:disable */
import { Character } from './character';
import { CategoryAttributeWrapper } from './category-attribute-wrapper';
import { ProductWrapper } from './product-wrapper';
export interface CategoryWrapper {
  id?: number;
  active?: boolean;
  activeStartDate?: string;
  archived?: Character;
  categoryAttributes?: Array<CategoryAttributeWrapper>;
  description?: string;
  activeEndDate?: string;
  longDescription?: string;
  name?: string;
  products?: Array<ProductWrapper>;
  subcategories?: Array<CategoryWrapper>;
  url?: string;
  urlKey?: string;
}
