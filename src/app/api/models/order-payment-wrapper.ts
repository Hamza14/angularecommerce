/* tslint:disable */
import { AddressWrapper } from './address-wrapper';
import { PaymentTransactionWrapper } from './payment-transaction-wrapper';
export interface OrderPaymentWrapper {
  active?: boolean;
  amount?: number;
  billingAddress?: AddressWrapper;
  currency?: string;
  gatewayType?: string;
  id?: number;
  orderId?: number;
  referenceNumber?: string;
  transactions?: Array<PaymentTransactionWrapper>;
  type?: string;
}
