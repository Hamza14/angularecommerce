/* tslint:disable */
export interface CartMessageWrapper {
  errorCode?: string;
  message?: string;
  messageType?: string;
  priority?: number;
}
