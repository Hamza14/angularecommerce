/* tslint:disable */
export interface Money {
  amount?: number;
  currency?: string;
  zero?: boolean;
}
