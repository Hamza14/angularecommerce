import {Component, OnInit} from '@angular/core';
import {CustomerWrapper} from '../api/models/customer-wrapper';
import {CustomerEndpointService} from '../api/services/customer-endpoint.service';
import {CustomCustomerEndpointService} from '../api/services/custom-customer-endpoint.service';
import AddCustomerUsingPOST1Params = CustomCustomerEndpointService.AddCustomerUsingPOST1Params;
import FindCustomerByEmailUsingGET1Params = CustomCustomerEndpointService.FindCustomerByEmailUsingGET1Params;

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {


  params(params: CustomerWrapper) {
    return params;
  }

  params1(params: AddCustomerUsingPOST1Params) {
    return params;
  }

  params2(params: FindCustomerByEmailUsingGET1Params) {
    return params;
  }

  customer: CustomerWrapper;
  private password: string;
  private passwordConfirm: string;
  private firstName: string;
  private email: string;
  private lastName: string;
  private customerId: string;

  constructor(private customCustomerEndpointService: CustomCustomerEndpointService,
              private customerEndpointService: CustomerEndpointService
  ) {
  }

  ngOnInit() {
    this.customerId = localStorage.getItem('customerId');
    this.customerEndpointService.findCustomerByEmailUsingGET3(localStorage.getItem('email'))
      .subscribe(customer => {
        this.customer = customer;
      });
  }

  addNewCustomer() {
    this.customerEndpointService.findCustomerByEmailUsingGET3(this.email)
      .subscribe(() => {
          alert('customer already exists ,please enter another email!');
        },
        err => {
          this.customCustomerEndpointService.addCustomerUsingPOST1(this.params1({
            wrapper: this.params({
              'firstName': this.firstName,
              'lastName': this.lastName,
              'emailAddress': this.email,
              'registered': true
            }), password: this.password
          }))
            .subscribe(() => {
              alert('Welcome ' + this.firstName);
              this.customerEndpointService.findCustomerByEmailUsingGET3(this.email)
                .subscribe(customer => {
                  localStorage.setItem('customerId', JSON.stringify(customer.id));
                  localStorage.setItem('email', this.email);
                  localStorage.setItem('name', customer.firstName);
                  window.location.reload();
                });
            });
        });
  }

  login() {
    this.customCustomerEndpointService.findCustomerByEmailUsingGET1(this.params2({email: this.email, password: this.password}))
      .subscribe(() => {

          this.customerEndpointService.findCustomerByEmailUsingGET3(this.email)
            .subscribe(customer => {
              localStorage.setItem('customerId', JSON.stringify(customer.id));
              localStorage.setItem('email', this.email);
              localStorage.setItem('name', customer.firstName);
              window.location.reload();
            });
        },
        error1 => {
          alert('Email and password doesn\'t match!');
        });

  }

  updateCustomer() {

  }
}
