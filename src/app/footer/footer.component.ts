import {Component, Input, OnInit} from '@angular/core';

import {CategoriesWrapper} from '../api/models/categories-wrapper';
import {CatalogEndpointService} from '../api/services/catalog-endpoint.service';
import FindAllCategoriesUsingGET1Params = CatalogEndpointService.FindAllCategoriesUsingGET1Params;
import {CategoryWrapper} from '../api/models/category-wrapper';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  @Input() categories : CategoriesWrapper;
private categorie : CategoryWrapper;


  constructor(private catalogEndpointService :CatalogEndpointService) { }
  params(params: FindAllCategoriesUsingGET1Params){
    return params;
  }

  ngOnInit() {

    this.catalogEndpointService.findAllCategoriesUsingGET1(this.params({"offset":null,"name":null,"limit":null}))
      .subscribe(data=>{
        this.categories=JSON.parse(JSON.stringify(data)).categories;
      });


  }
}
